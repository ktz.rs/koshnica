import { Meteor } from 'meteor/meteor';
import '/imports/startup/client/routes';
import '/imports/startup/client/fixtures';

import 'bootstrap';

Template.header.events({
    'click #e-openMenu': (event) => {
        $(event.currentTarget).toggleClass('is-active');
        $('nav').toggleClass('is-visible');
    },
    'click #e-signOut': () => {
        // Sign out
        Meteor.logout();
    } 
});
