import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';
import { Accounts } from 'meteor/accounts-base';
import '/imports/startup/server/index';
import '/lib/schema';

Accounts.onCreateUser((options, user) => {
    // Facebook OAuth
    if (user.services.facebook) {
        if (Meteor.users.find({
                $or: [{
                    'profile.email': new RegExp(user.services.facebook.email, 'i')
                }, {
                    'service.facebook.email': new RegExp(user.services.facebook.email, 'i')
                }, {
                    'emails.address': new RegExp(user.services.facebook.email, 'i')
                }]
            }).fetch().length > 0) {
            throw new Meteor.Error('Грешка!', 'Ова адреса е-поште већ постоји у нашој бази. Уколико не можеш да се сетиш лозинке, можеш је обновити.');
        }
        // Get the data from additional permissions
        let result = HTTP.get(`https://graph.facebook.com/v3.2/${user.services.facebook.id}?access_token=${user.services.facebook.accessToken}&fields=name,birthday,location,gender`);
        let data = result.data; 
        
        // Add the data to user object
        user.profile = {
            name: user.services.facebook.name,
            gender: data.gender === 'male' ? 'Мушко' : 'Женско',
            birth: data.birthday,
            picture: {
                link: `https://graph.facebook.com/${user.services.facebook.id}/picture?type=large`
            },
            city: data.location.name
        };
        user.emails = [{
            address: user.services.facebook.email,
            verified: true
        }]
    }

    // LinkedIn OAuth
    if (user.services.linkedin) {
        if (Meteor.users.find({
                $or: [{
                    'profile.email': new RegExp(user.services.linkedin.email, 'i')
                }, {
                    'service.linkedin.email': new RegExp(user.services.linkedin.email, 'i')
                }, {
                    'emails.address': new RegExp(user.services.linkedin.email, 'i')
                }]
            }).fetch().length > 0) {
            throw new Meteor.Error('Грешка!', 'Ова адреса е-поште већ постоји у нашој бази. Уколико не можеш да се сетиш лозинке, можеш је обновити.');
        }
        // Get the data from additional permissions
        let result = HTTP.get('https://api.linkedin.com/v2/me', {
            headers: {
                'Authorization': `Bearer ${user.services.linkedin.accessToken}`
            }
        });
        let data = result.data;

        // Add the data to the user object
        user.profile = {
            name: `${user.services.linkedin.firstName.localized.en_US} ${user.services.linkedin.lastName.localized.en_US}`,
            picture: {
                link: user.services.linkedin.profilePicture.identifiersUrl[3]
            }
        }
        user.emails = [{
            address: user.services.linkedin.email,
            verified: true
        }]

        user.flags = {
            registerState: 'registerPlaceAndSchool'
        }
        // Create user
        return user;
    }

    user.profile = options.profile
    return user;
});

// Disallow any user data modifications from the client side
Meteor.users.deny({
    update: () => true,
    remove: () => true,
    insert: () => true
});
