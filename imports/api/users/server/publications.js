Meteor.publish('user.info', function profilePublish() {
  if (!this.userId) {
      return this.ready();
  }
  return Meteor.users.find({
      _id: this.userId
  }, {
      fields: {
          emails: 1,
          profile: 1,
          'services.facebook.email': 1,
          flags: 1
      },
  });
});
