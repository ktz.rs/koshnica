import { Accounts } from 'meteor/accounts-base';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import { UV_UDP_REUSEADDR } from 'constants';

SimpleSchema.extendOptions(['autoform']);

let UserSchema = {};

UserSchema.whole = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  createdAt: {
    type: Date,
    optional: true,
  },
  events: {
    type: Array,
    optional: true
  },
  'events.$': {
    type: Object,
    optional: true
  },
  'events.$.finishedSignUp': {
    type: Boolean,
    optional: true,
  },
  flags: {
    type: Object,
    blackbox: true,
    optional: true
  },
  'flags.registerState': {
    type: String,
    optional: true
  },
  services: {
    type: Object,
    blackbox: true
  },
  'services.password': {
    type: Object,
    blackbox: true
  },
  profile: Object,
  'profile.city': {
    type: Number,
    label: 'Град',
    optional: true,
  },
  'profile.abroadCity': {
    type: String,
    label: 'Град у иностранству',
    optional: true
  },
  'profile.primarySchoolCity': {
    type: Number,
    optional: true
  },
  'profile.highSchoolCity': {
    type: Number,
    optional: true
  },
  'profile.highSchool': {
    type: Number,
    optional: true
  },
  'profile.highSchoolMajor': {
    type: Number,
    optional: true
  },
  'profile.facultyCity': {
    type: Number,
    optional: true
  },
  'profile.faculty': {
    type: Number,
    optional: true
  },
  'profile.facultyMajor': {
    type: Number,
    optional: true
  },
  'profile.abroadFacultyCity': {
    type: String,
    optional: true
  },
  'profile.abroadFaculty': {
    type: String,
    optional: true
  },
  'profile.abroadFacultyMajor': {
    type: String,
    optional: true
  },
  'profile.workingProfession': {
    type: String,
    optional: true
  },
  'profile.picture': {
    type: Object,
    optional: true
  },
  'profile.picture.link': {
    type: String,
    optional: true
  },
  'profile.picture.deleteHash': {
    type: String,
    optional: true
  },
  'profile.qualification': {
    type: Number,
    optional: true,
  },
  'profile.workingCity': {
    type: Number,
    optional: true,
  },
  'profile.abroadWorkingCity': {
    type: String,
    optional: true,
  },
  'profile.languages': {
    type: Array,
    optional: true,
  },
  'profile.languages.$': {
    type: String,
    optional: true,
  },
  'profile.phone': {
      type: String,
      min: 10,
      max: 17,
      optional: true,
  },
})

UserSchema.general = new SimpleSchema({
  profile: Object,
  'profile.name': {
    type: String,
    label: 'Име и презиме',
    min: 10,
    max: 50
  },
  'profile.gender': {
    type: String,
    label: 'Пол',
    allowedValues: ['Мушко', 'Женско'],
    optional: true,
  },
  'profile.birth': {
    type: String,
    label: 'Датум рођења',
    optional: true,
  },
  emails: {
    type: Array,
    optional: true
  },
  'emails.$': {
    type: Object,
  },
  'emails.$.address': {
    type: SimpleSchema.RegEx.Email,
    label: "Е-пошта",
    max: 50
  },
  'emails.$.verified': {
    type: Boolean,
  },
  password: {
    type: String,
    label: "Лозинка",
    min: 6,
    max: 20,
    optional: true,
  },
}, { tracker: Tracker })

UserSchema.placeAndSchool = new SimpleSchema({
  profile: Object,
  'profile.city': {
    type: Number,
    label: 'Град',
    optional: true
  },
  'profile.abroadCity': {
    type: String,
    label: 'Град у иностранству',
    optional: true
  },
  'profile.primarySchoolCity': Number,
  'profile.highSchoolCity': Number,
  'profile.highSchool': Number,
  'profile.highSchoolMajor': Number
}, { tracker: Tracker })

UserSchema.faculty = new SimpleSchema({
  profile: Object,
  'profile.facultyCity': {
    type: Number,
    optional: true
  },
  'profile.faculty': {
    type: Number,
    optional: true
  },
  'profile.facultyMajor': {
    type: Number,
    optional: true
  },
  'profile.abroadFacultyCity': {
    type: String,
    optional: true
  },
  'profile.abroadFaculty': {
    type: String,
    optional: true
  },
  'profile.abroadFacultyMajor': {
    type: String,
    optional: true
  },
  'profile.workingProfession': String
}, { tracker: Tracker })

UserSchema.media = new SimpleSchema({
  picture: {
    type: String,
    label: 'Отпреми фотографију',
    optional: true,
  },
  biography: {
    type: String,
    label: 'Отпреми биографију',
    optional: true
  }
})

UserSchema.lastData = new SimpleSchema({
  'profile': Object,
  'profile.qualification': {
    type: Number,
  },
  'profile.workingCity': {
    type: Number,
    optional: true,
  },
  'profile.abroadWorkingCity': {
    type: String,
    optional: true,
  },
  'profile.languages': {
    type: Array,
  },
  'profile.languages.$': {
    type: String
  },
  'profile.phone': {
      type: String,
      min: 10,
      max: 17
  },
});

UserSchema.accountSettings = new SimpleSchema({
  'profile': {
    type: Object,
  },
  'profile.picture': {
    type: Object,
    optional: true
  },
  'profile.picture.link': {
    type: String,
    optional: true
  },
  'profile.picture.deleteHash': {
    type: String,
    optional: true
  },
  'profile.name': String,
  'profile.gender': {
    type: String,
    label: 'Пол',
    allowedValues: ['Мушко', 'Женско']
  },
  'profile.birth': {
    type: String,
    label: 'Датум рођења'
  },
  emails: {
    type: Array,
    optional: true
  },
  'emails.$': {
    type: Object,
  },
  'emails.$.address': {
    type: SimpleSchema.RegEx.Email,
    label: "Е-пошта",
    max: 50
  },
  'profile.phone': String,
  'profile.city': {
    type: Number,
    label: 'Град',
    optional: true
  },
  'profile.workingCity': {
    type: Number,
    optional: true,
  },
})

UserSchema.schoolSettings = new SimpleSchema({
  'profile': {
    type: Object,
  },
  'profile.highSchoolCity': {
    type: Number,
    label: 'Град',
    optional: true
  },
  'profile.highSchool': {
    type: Number,
    optional: true
  },
  'profile.highSchoolMajor': {
    type: Number,
    optional: true
  },
  'profile.facultyCity': {
    type: Number,
    optional: true,
  },
  'profile.faculty': {
    type: Number,
    optional: true,
  },
  'profile.facultyMajor': {
    type: Number,
    optional: true,
  },
})

UserSchema.whole.extend(UserSchema.general)

Meteor.users.attachSchema(UserSchema.whole);

export default UserSchema;

if (Meteor.isServer) {
  Accounts.validateNewUser((user) => {
    UserSchema.whole.validate(user);

    return true;
  });
}
