import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { _ } from 'meteor/underscore';
import SimpleSchema from 'simpl-schema';
import UserSchema from './users';
import { Accounts } from 'meteor/accounts-base';

const changeFlagOfRegistration = new ValidatedMethod({
    name: 'changeFlagOfRegistration',
    validate: new SimpleSchema({
        registerState: String,
    }).validator({ clean: true }),
    run({ registerState }) {
        if (!this.userId) {
            return false;
        }
        return Meteor.users.update({ _id: this.userId }, { $set: { 'flags.registerState': registerState } });
    },
});

const registerPlaceAndSchool = new ValidatedMethod({
    name: 'registerPlaceAndSchool',
    validate: UserSchema.placeAndSchool.validator({ clean: true }),
    run({ profile }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.city': profile.city,
                'profile.abroadCity': profile.abroadCity,
                'profile.primarySchoolCity': profile.primarySchoolCity,
                'profile.highSchoolCity': profile.highSchoolCity,
                'profile.highSchool': profile.highSchool,
                'profile.highSchoolMajor': profile.highSchoolMajor
            }
        });
    },
});

const registerFaculty = new ValidatedMethod({
    name: 'registerFaculty',
    validate: UserSchema.faculty.validator({ clean: true }),
    run({ profile }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.facultyCity': profile.facultyCity,
                'profile.faculty': profile.faculty,
                'profile.facultyMajor': profile.facultyMajor,
                'profile.abroadFacultyCity': profile.abroadFacultyCity,
                'profile.abroadFaculty': profile.abroadFaculty,
                'profile.abroadFacultyMajor': profile.abroadFacultyMajor,
                'profile.workingProfession': profile.workingProfession,
            }
        });
    },
});

const registerMedia = new ValidatedMethod({
    name: 'registerMedia',
    validate: new SimpleSchema({
        link: String,
        deleteHash: String
    }).validator({ clean: true }),
    run({ link, deleteHash }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.picture.link': link,
                'profile.picture.deleteHash': deleteHash,
            }
        });
    },
});

const registerLast = new ValidatedMethod({
    name: 'registerLast',
    validate: UserSchema.lastData.validator({ clean: true }),
    run({ profile }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.qualification': profile.qualification,
                'profile.workingCity': profile.workingCity,
                'profile.abroadWorkingCity': profile.abroadWorkingCity,
                'profile.languages': profile.languages,
                'profile.phone': profile.phone
            }
        });
    },
});

const saveUserToApi = new ValidatedMethod({
    name: 'saveUserToApi',
    validate: null,
    run() {
        if (!this.userId) {
            return false;
        }

        const currentUser = Meteor.users.findOne({ _id: this.userId });
        let apiUrl = 'https://kosnica.ktz.rs';

        if (Meteor.isDevelopment) {
            apiUrl = 'http://localhost:5000'
        }

        HTTP.call('POST', `${apiUrl}/rest/koshnica_users`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${Meteor.settings.public.api.jwtToken}`,
                'Accept': 'application/vnd.pgrst.object+json',
                'Prefer': 'return=representation',
            },
            data: {
                user_hash: this.userId,
                gender: currentUser.profile.gender,
                birth_date: currentUser.profile.birth,
                city: currentUser.profile.city,
                abroad_city: currentUser.profile.abroadCity,
                primary_school_city: currentUser.profile.primarySchoolCity,
                high_school_city: currentUser.profile.highSchoolCity,
                faculty_city: currentUser.profile.facultyCity,
                high_school: currentUser.profile.highSchool,
                high_school_major: currentUser.profile.highSchoolMajor,
                faculty: currentUser.profile.faculty,
                faculty_major: currentUser.profile.facultyMajor,
                working_profession: currentUser.profile.workingProfession,
                abroad_faculty_city: currentUser.profile.abroadFacultyCity,
                abroad_faculty: currentUser.profile.abroadFaculty,
                abroad_faculty_major: currentUser.profile.abroadFacultyMajor,
                qualification: currentUser.profile.qualification,
                working_city: currentUser.profile.workingCity,
                abroad_working_city: currentUser.profile.abroadWorkingCity,
                languages: currentUser.profile.languages
            }
        }, (err, data) => {
            if (err) {
                console.log(err)
            }
            console.log(data)
        });
    },
});

const changeEmail = new ValidatedMethod({
    name: 'changeEmail',
    validate: new SimpleSchema({
        email: String
    }).validator({ clean: true }),
    run({ email }) {
        if (!this.userId) {
            return false;
        }

        var user = Meteor.user();
        var oldemail = user.emails;
        if (oldemail != null) {
            Accounts.removeEmail(user._id, user.emails[0].address)
        }
        Accounts.addEmail(user._id, email);
        // Accounts.sendVerificationEmail(user._id);
        return email;
    },
});

const accountSettings = new ValidatedMethod({
    name: 'accountSettings',
    validate: UserSchema.accountSettings.validator({ clean: true }),
    run({ profile }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.name': profile.name,
                'profile.gender': profile.gender,
                'profile.birth': profile.birth,
                'profile.phone': profile.phone,
                'profile.city': profile.city,
                'profile.workingCity': profile.workingCity,
            }
        }, (err, data) => {
            let apiUrl = 'https://kosnica.ktz.rs';

            if (Meteor.isDevelopment) {
                apiUrl = 'http://localhost:5000'
            }

            HTTP.call('PATCH', `${apiUrl}/rest/koshnica_users?user_hash=eq.${this.userId}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${Meteor.settings.public.api.jwtToken}`,
                    'Accept': 'application/vnd.pgrst.object+json',
                    'Prefer': 'return=representation',
                },
                data: {
                    gender: profile.gender,
                    birth_date: profile.birth,
                    city: profile.city,
                    working_city: profile.workingCity
                }
            }, (err, data) => {
                if (err) {
                    console.log(err)
                }
                console.log(data)
            });
        });
    },
})

const schoolSettings = new ValidatedMethod({
    name: 'schoolSettings',
    validate: UserSchema.schoolSettings.validator({ clean: true }),
    run({ profile }) {
        if (!this.userId) {
            return false;
        }

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'profile.highSchoolCity': profile.highSchoolCity,
                'profile.highSchool': profile.highSchool,
                'profile.highSchoolMajor': profile.highSchoolMajor,
                'profile.facultyCity': profile.facultyCity,
                'profile.faculty': profile.faculty,
                'profile.facultyMajor': profile.facultyMajor,
            }
        }, (err, data) => {
            let apiUrl = 'https://kosnica.ktz.rs';

            if (Meteor.isDevelopment) {
                apiUrl = 'http://localhost:5000'
            }

            HTTP.call('PATCH', `${apiUrl}/rest/koshnica_users?user_hash=eq.${this.userId}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${Meteor.settings.public.api.jwtToken}`,
                    'Accept': 'application/vnd.pgrst.object+json',
                    'Prefer': 'return=representation',
                },
                data: {
                    high_school_city: profile.highSchoolCity,
                    faculty_city: profile.facultyCity,
                    high_school: profile.highSchool,
                    high_school_major: profile.highSchoolMajor,
                    faculty: profile.faculty,
                    faculty_major: profile.facultyMajor,
                }
            }, (err, data) => {
                if (err) {
                    console.log(err)
                }
                console.log(data)
            });
        });
    },
})

// Get list of all method names on Jobs
const USERS_METHODS = _.pluck([
    changeFlagOfRegistration,
    registerPlaceAndSchool,
    registerFaculty,
    registerMedia,
    registerLast,
    saveUserToApi,
    changeEmail,
    accountSettings,
    schoolSettings
], 'name');

// Only allow 5 articles operations per connection per second
DDPRateLimiter.addRule({
    name(name) {
        return _.contains(USERS_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
}, 5, 1000);
