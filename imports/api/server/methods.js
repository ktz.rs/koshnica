import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Roles } from 'meteor/alanning:roles';
import SimpleSchema from 'simpl-schema';
import { userDocuments } from '/lib/collections';

const checkEmail = new ValidatedMethod({
    name: 'checkEmail',
    validate: new SimpleSchema({
        email: { type: String }
    }).validator(),
    run({ email }) {
        let emailExists = Meteor.users.find({ 'emails.address': email }).fetch().length;
        if (emailExists > 0) {
            return new Meteor.Error('Грешка!','Ова адреса е-поште је већ регистрована.');
        }
    }
});

const exportCSV = new ValidatedMethod({
    name: 'exportCSV',
    validate: null,
    run() {
        const users = Meteor.users.find({}).fetch();
        const profile = users.map((data) => {
            return {
                fullName: data.profile.name,
                gender: data.profile.gender,
                birth: data.profile.birth,
                city: data.profile.city,
                highSchool: data.school.high.name,
                highSchoolClass: data.school.high.class,
                faculty: data.school.faculty,
                facultyClass: data.school.facultyClass,
                email: data.emails[0].address,
                image: data.profile.picture.link
            };
        });
        return profile;
    }
});

const exportAnoData = new ValidatedMethod({
    name: 'exportAnoData',
    validate: null,
    run() {
        const users = Meteor.users.find({}).fetch();
        const profiles = users.map((data, index) => {
            return {
                'Редни број': index + 1,
                'Име и презиме': `Пчела ${index + 1}`,
                'Пол': data.profile.gender,
                'Датум рођења': new Intl.DateTimeFormat('SR', {
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric'
                }).format(new Date(data.profile.birth)),
                'Место становања': data.profile.city,
                'Град школе (Средња школа)': data.school.high.city,
                'Средња школа': data.school.high.name,
                'Средња школа (Смер)': data.school.high.class,
                'Град школе (Факултет)': data.school.faculty.city,
                'Факултет': data.school.faculty.name,
                'Факултет (Смер)': data.school.faculty.class,
                'Занимање': data.occupation.name,
                'Радни статус': data.occupation.employed ? 'Запослена пчела' : 'Незапослена пчела'
            };
        });
        return profiles;
    }
});

const analyzeData = new ValidatedMethod({
    name: 'analyzeDaza',
    validate: null,
    run() {
        const users = Meteor.users.find({}).fetch();
        const profiles = users.map((data) => {
            return {
                gender: data.profile.gender,
                birth: data.profile.birth,
                city: data.profile.city,
                highSchool: data.school.high,
                faculty: data.school.faculty,
                languages: data.languages,
                occupation: data.occupation
            };
        });
        return profiles;
    }
});

const deleteProfile = new ValidatedMethod({
    name: 'deleteProfile',
    validate: new SimpleSchema({
        userId: { type: String }
    }).validator(),
    run({ userId }) {
        if (this.userId === userId || Roles.userIsInRole(this.userId, 'user:delete')) {
            // Check if the user has uploaded its own image
            let ImgurData = Meteor.users.findOne({ _id: userId }).profile.picture.deleteHash;

            // The user has an image on Imgur
            if (ImgurData !== undefined) {
                // Get the imgur delete hash
                let deleteHash = Meteor.users.findOne({ _id: userId }).profile.picture.deleteHash;
                // Delete the profile image from Imgur
                Meteor.call('imgur.delete', { deleteHash }, (err) => {
                    if (!err) {
                        // Delete user's CV
                        userDocuments.remove({ userId });
                        // Delete the user and all of its data from the database
                        Meteor.users.remove({ _id: userId });
                    } else {
                        console.error(err);
                    }
                });
            } else {
                // Delete user's CV
                userDocuments.remove({ userId });
                // Delete the user and all of its data from the database
                Meteor.users.remove({ _id: userId });
            }
        }
    }
});

// User update flow
const updateProfile = new ValidatedMethod({
    name: 'updateProfile',
    validate: new SimpleSchema({
        query: { type: String },
        selector: { type: String }
    }).validator(),
    run({ query, selector }) {
        Meteor.users.update({ _id: this.userId }, {
            $set: {
                [selector]: query
            }
        });
        let updateInfo = Meteor.users.findOne({ _id: this.userId })[selector];
        return updateInfo;
    }
});

// Account creation flow
const registerPersonalData = new ValidatedMethod({
    name: 'registerPersonalData',
    validate: new SimpleSchema({
        name: { type: String },
        gender: { type: String }, 
        birth: { type: String }, 
        city: { type: String },
        phone: { type: String }
    }).validator(),
    run({ name, gender, birth, city, phone }) {
        try {
            Meteor.users.upsert({ _id: this.userId } , {
                $set: {
                    'profile': {
                        name,
                        gender,
                        birth,
                        city,
                        phone
                    }
                }
            }, { multi: true }, (err) => {
                if (err) {
                    console.error(err);
                }
            });
        } catch (error) {
            return new Meteor.Error('Грешка!', `Грешка током ажурирања налога. ${error.reason}`);
        }
    }
});

const registerSchool = new ValidatedMethod({
    name: 'registerSchool',
    validate: new SimpleSchema({
        highCity: { type: String },
        highName: { type: String }, 
        highClass: { type: String },
        facultyCity: { type: String },
        facultyName: { type: String },
        facultyClass: { type: String }
    }).validator(),
    run({ highCity, highName, highClass, facultyCity, facultyName, facultyClass }) {
        try {
            Meteor.users.upsert({ _id: this.userId }, {
                $set: {
                    'school.high': {
                            city: highCity,
                            name: highName,
                            class: highClass
                    },
                    'school.faculty': {
                            city: facultyCity,
                            name: facultyName,
                            class: facultyClass
                        }
                    }
            }, { multi: true }, (err) => {
                console.log(err);
            });
        } catch (error) {
            return new Meteor.Error('Грешка!', `Грешка током ажурирања налога. ${error.reason}`);
        }
    }
});

const addOccupation = new ValidatedMethod({
    name: 'addOccupation',
    validate: new SimpleSchema({
        name: { type: String },
        employed: { type: Boolean }
    }).validator(),
    run({ name, employed }) {
        try {
            Meteor.users.upsert({ _id: this.userId }, {
                $set: {
                    'occupation': {
                        name,
                        employed
                    }
                }
            }, { multi: true }, (err) => {
                console.error(err);
            })
        } catch (error) {
            
        }
    }
});

// const registerMedia = new ValidatedMethod({
//     name: 'registerMedia',
//     validate: new SimpleSchema({
//         link: { type: String },
//         deleteHash: { type: String }
//     }).validator(),
//     run({ link, deleteHash }) {
//         try {
//             Meteor.users.upsert({ _id: this.userId }, {
//                 $set: {
//                     'profile.picture': {
//                         link,
//                         deleteHash
//                     }
//                 }
//             }, { multi: true }, (err) => {
//                 if (err) {
//                     console.error(err)
//                 }
//             });
//         } catch (error) {
//             return new Meteor.Error('Грешка!', `Грешка током ажурирања налога. ${error.reason}`);
//         }
//     }
// });

export {
    checkEmail,
    exportCSV,
    exportAnoData,
    analyzeData,
    deleteProfile,
    registerPersonalData,
    registerSchool,
    registerMedia,
    updateProfile,
    addOccupation
}
