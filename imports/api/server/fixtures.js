import { Meteor } from 'meteor/meteor';

ServiceConfiguration.configurations.upsert({
    service: 'facebook'
}, {
    service: 'facebook',
    appId: Meteor.settings.public.Facebook.appID,
    secret: Meteor.settings.public.Facebook.appSecret
})

ServiceConfiguration.configurations.upsert({
    service: 'linkedin'
}, {
    $set: {
      clientId: Meteor.settings.public.LinkedIn.appID,
      secret: Meteor.settings.public.LinkedIn.appSecret
    }
})

Accounts.urls.resetPassword = (token) => Meteor.absoluteUrl(`reset/${token}`);