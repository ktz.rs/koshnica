import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { userDocuments } from '/lib/collections';

// User data
Meteor.publish('user', function (userId) {
    if (this.userId === userId) {
        return Meteor.users.find({ _id: userId });
    }
});

// Get the user's CV
Meteor.publish('userDocuments', function (userId) {
    if (this.userId === userId && Roles.userIsInRole(this.userId, 'db:read')) {
        return userDocuments.find().cursor;
    }
});

// Display the data from all users
Meteor.publish('usersCollection', function (userId) {
    if (this.userId === userId && Roles.userIsInRole(this.userId, 'db:read')) {
        return Meteor.users.find();
    }
});

// Get the values for charts
Meteor.publish('charts', function() {
    return Meteor.users.find({}, {
        fields: {
            createdAt: 0,
            services: 0,
            email: 0,
            emails: 0,
            'profile.name': 0,
            'profile.phone': 0,
            'profile.picture': 0,
            roles: 0
        }
    })
});
