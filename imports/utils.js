const charMap = new Map([
  ['Lj', 'Љ'],
  ['LJ', 'Љ'],
  ['Nj', 'Њ'],
  ['NJ', 'Њ'],
  ['Dž', 'Џ'],
  ['DŽ', 'Џ'],
  ['Dj', 'Ђ'],
  ['DJ', 'Ђ'],
  ['lj', 'љ'],
  ['nj', 'њ'],
  ['dž', 'џ'],
  ['dj', 'ђ'],
  ['A', 'А'],
  ['B', 'Б'],
  ['V', 'В'],
  ['G', 'Г'],
  ['D', 'Д'],
  ['Đ', 'Ђ'],
  ['E', 'Е'],
  ['Ž', 'Ж'],
  ['Z', 'З'],
  ['I', 'И'],
  ['J', 'Ј'],
  ['K', 'К'],
  ['L', 'Л'],
  ['M', 'М'],
  ['N', 'Н'],
  ['O', 'О'],
  ['P', 'П'],
  ['R', 'Р'],
  ['S', 'С'],
  ['T', 'Т'],
  ['Ć', 'Ћ'],
  ['U', 'У'],
  ['F', 'Ф'],
  ['H', 'Х'],
  ['C', 'Ц'],
  ['Č', 'Ч'],
  ['Š', 'Ш'],
  ['a', 'а'],
  ['b', 'б'],
  ['v', 'в'],
  ['g', 'г'],
  ['d', 'д'],
  ['đ', 'ђ'],
  ['e', 'е'],
  ['ž', 'ж'],
  ['z', 'з'],
  ['i', 'и'],
  ['j', 'ј'],
  ['k', 'к'],
  ['l', 'л'],
  ['m', 'м'],
  ['n', 'н'],
  ['o', 'о'],
  ['p', 'п'],
  ['r', 'р'],
  ['s', 'с'],
  ['t', 'т'],
  ['ć', 'ћ'],
  ['u', 'у'],
  ['f', 'ф'],
  ['h', 'х'],
  ['c', 'ц'],
  ['č', 'ч'],
  ['š', 'ш']
])

export const latin2cyrillic = (text, map = charMap) => {
  if (typeof text !== 'string') {
      console.log(text)
      throw new Error('Invalid input data type')
  }
  // Finds every key occurance in text and replaces it with replacement value using RegExp
  t = text
  for (let k of map.keys()) {
      text = text.replace(RegExp(k, 'gu'), map.get(k))
  }
  return text
}
