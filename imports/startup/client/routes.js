import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { Roles } from 'meteor/alanning:roles';
import '/imports/ui/components/header/header.js';
import '/imports/ui/layout.js';
import '/imports/ui/components/loading/loading.html';
import '/imports/ui/components/404/404.html';

// Home
FlowRouter.route('/', {
    name: 'home',
    triggersEnter: [(context, redirect, stop, user) => {
        if (user && user.flags.registerState !== 'done') {
            redirect('register');
        }
    }],
    waitOn() {
        return [
            import('/imports/ui/pages/graphs/graphs.js'), 
            Meteor.subscribe('user.info'),
            Meteor.subscribe('user', Meteor.userId())
        ];
    },
    whileWaiting() {
        this.render('loading');
    },
    data() {
        return  Meteor.users.findOne({ _id: Meteor.userId() })
    },
    action() {
        this.render('layout', 'graphs');
    }
});

FlowRouter.route('/poslednja', {
    name: 'poslednjaTest',
    waitOn() {
        return import('/imports/ui/pages/register/registerLast.js')
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'registerLast');
    }
});


// Grafovi
FlowRouter.route('/uvidi', {
    name: 'graphs',
    waitOn() {
        return [import('/imports/ui/pages/home/home.js'), Meteor.subscribe('user', Meteor.userId())]
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'home');
    }
});

FlowRouter.route('/mini-uvidi', {
    name: 'detailedGraphs',
    waitOn() {
        return [import('/imports/ui/pages/graphs/detailedGraphs.js'), Meteor.subscribe('user', Meteor.userId())]
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'detailedGraphs');
    }
});

// Register
FlowRouter.route('/registracija', {
    name: 'register',
    waitOn() {
        return [
            import('/imports/ui/components/modal/tos.js'),
            import('/imports/ui/components/modal/modal.js'),
            import('/imports/ui/pages/register/register.js'),
            import('/imports/ui/pages/register/registerAccount.js'),
            import('/imports/ui/pages/register/registerPlaceAndSchool.js'),
            import('/imports/ui/pages/register/registerFaculty.js'),
            import('/imports/ui/pages/register/registerMedia.js'),
            import('/imports/ui/pages/register/registerBiography.js'),
            import('/imports/ui/pages/register/registerLast.js'),
            import('/imports/ui/pages/home/home.js'),
            Meteor.subscribe('user.info')
        ]
    },
    whileWaiting() {
        this.render('loading');
    },
    data() {
        if (Meteor.userId()) {
            return Meteor.users.findOne({ _id: Meteor.userId() }).flags.registerState;
        }
    },
    action(params, qs, data) { 
        this.render('layout', 'register', { data });
    }
});

// Sign in
FlowRouter.route('/prijava', {
    name: 'signIn',
    waitOn() {
        return [ 
            import('/imports/ui/components/modal/modal.js'),
            import('/imports/ui/pages/signIn/signIn.js'),
        ]
    },
    whileWaiting() {
        this.render('loading');
    },
    action() { 
        if (!Meteor.userId()) {
            this.render('layout', 'signIn');
        } else {
            FlowRouter.go('home');
        }
    }
});

// Reset pass
FlowRouter.route('/reset', {
    name: 'reset',
    waitOn() {
        return [ 
            import('/imports/ui/components/modal/modal.js'),
            import('/imports/ui/pages/reset/reset.js'),
        ]
    },
    whileWaiting() {
        this.render('loading');
    },
    action() { 
        if (!Meteor.userId()) {
            this.render('layout', 'reset');
        } else {
            FlowRouter.go('home');
        }
    }
});

// Profile
FlowRouter.route('/izmena-profila', {
    name: 'settings',
    waitOn() {
        return [
            import('/imports/ui/pages/profile/profile.html'),
            import('/imports/ui/pages/profile/profile.js'),
            Meteor.subscribe('user', Meteor.userId())
        ]
    },
    whileWaiting() {
        this.render('loading');
    },
    action(param, qa, user) {
        if (Meteor.userId()) {
            this.render('layout', 'profile', { user });
        } else {
            FlowRouter.go('home');
        }
    }
});

// Dashboard
FlowRouter.route('/dashboard', {
    name: 'dashboard',
    waitOn() {
        return [
            import('/imports/ui/pages/dashboard/dashboard.js'),
            import('/imports/ui/pages/home/home.js'),
            Meteor.subscribe('usersCollection', Meteor.userId()),
            Meteor.subscribe('userDocuments', Meteor.userId()),
            Meteor.subscribe('user', Meteor.userId())
        ]
    },
    whileWaiting() {
        this.render('loading');
    },
    action(params, qs, users) {
        if (Meteor.userId() && Roles.userIsInRole(Meteor.userId(), 'db:read')) {
            this.render('layout', 'dashboard', { users });
        } else {
            this.render('layout', 'home');
        } 
    },
    data() {
        return Meteor.users.find({}).fetch();
    }
});

// Data visualization
FlowRouter.route('/analiza', {
    name: 'analiza',
    waitOn() {
        return import('/imports/ui/pages/analyze/analyze.js')
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'analyze');
    }
});

// Map
FlowRouter.route('/mapa', {
    name: 'mapa',
    waitOn() {
        return import('/imports/ui/pages/map/map.js')
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'mapGraph');
    }
});

FlowRouter.route('/o-koshnici', {
    name: 'about',
    waitOn() {
        return import('/imports/ui/pages/about/about.js')
    },
    whileWaiting() {
        this.render('loading');
    },
    action() {
        this.render('layout', 'about');
    }
});

// 404
FlowRouter.route('*', {
    name: 'notFound',
    action() {
        this.render('layout', '404');
    }
});
