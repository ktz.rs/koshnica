import { openDB } from 'idb';
import { HTTP } from 'meteor/http';
import { DBNAME, DBVERSION } from '/imports/constants';

let apiUrl = 'https://kosnica.ktz.rs';

Meteor.startup(async function () {
  //check for support
  if (!('indexedDB' in window)) {
    console.log('This browser doesn\'t support IndexedDB');
    return;
  }

  if (Meteor.isDevelopment) {
    apiUrl = 'http://localhost:5000'
  }

  (async () => {
    const db = await openDB(DBNAME, DBVERSION, {
      async upgrade(db, oldVersion, newVersion, transaction) {
        //Removing current stores
        if (db.objectStoreNames.contains('cities')) {
          db.deleteObjectStore('cities');
        }

        if (db.objectStoreNames.contains('faculties')) {
          db.deleteObjectStore('faculties');
        }

        if (db.objectStoreNames.contains('facultyMajors')) {
          db.deleteObjectStore('facultyMajors');
        }

        if (db.objectStoreNames.contains('universities')) {
          db.deleteObjectStore('universities');
        }

        if (db.objectStoreNames.contains('highSchools')) {
          db.deleteObjectStore('highSchools');
        }

        if (db.objectStoreNames.contains('highSchoolMajors')) {
          db.deleteObjectStore('highSchoolMajors');
        }

        if (db.objectStoreNames.contains('qualifications')) {
          db.deleteObjectStore('qualifications');
        }

        // Creating new stores
        const citiesStore = db.createObjectStore('cities', { keyPath: 'id' });
        const facultiesStore = await db.createObjectStore('faculties', { keyPath: 'id' });
        const facultyMajorsStore = await db.createObjectStore('facultyMajors', { keyPath: 'id' });
        const universitiesStore = await db.createObjectStore('universities', { keyPath: 'id' });
        const highSchoolStore = await db.createObjectStore('highSchools', { keyPath: 'id' });
        const highSchoolMajorsStore = await db.createObjectStore('highSchoolMajors', { keyPath: 'id' });
        const qualificationsStore = await db.createObjectStore('qualifications', { keyPath: 'id' });

        // Setting indexes
        citiesStore.createIndex('name', 'name');
        facultiesStore.createIndex('name', 'name');
        facultyMajorsStore.createIndex('name', 'name');
        facultyMajorsStore.createIndex('facultyId', 'facultyId');
        universitiesStore.createIndex('name', 'name');
        highSchoolStore.createIndex('name', 'name');
        highSchoolMajorsStore.createIndex('name', 'name');
        highSchoolMajorsStore.createIndex('schoolId', 'schoolId');
        qualificationsStore.createIndex('name', 'name');
      }
    });

    const cities = await db.getAllFromIndex('cities', 'name');

    if (!cities.length) {
      HTTP.get(`${apiUrl}/rest/cities`, async function (err, data) {
        if (!err) {
          const tx = await db.transaction('cities', 'readwrite');
          data.data.forEach(city => {
            tx.store.add({
              id: city.id,
              name: city.name,
              postalCode: city.postal_code
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const faculties = await db.getAllFromIndex('faculties', 'name');

    if (!faculties.length) {
      HTTP.get(`${apiUrl}/rest/faculties`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('faculties', 'readwrite');
          data.data.forEach(faculty => {
            tx.store.add({
              id: faculty.id,
              name: faculty.name,
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const facultyMajors = await db.getAllFromIndex('facultyMajors', 'name');

    if (!facultyMajors.length) {
      HTTP.get(`${apiUrl}/rest/faculty_majors`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('facultyMajors', 'readwrite');
          data.data.forEach(major => {
            tx.store.add({
              id: major.id,
              name: major.name,
              facultyId: major.faculty_id
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const universities = await db.getAllFromIndex('universities', 'name');

    if (!universities.length) {
      HTTP.get(`${apiUrl}/rest/universities`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('universities', 'readwrite');
          data.data.forEach(university => {
            tx.store.add({
              id: university.id,
              name: university.name,
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const highSchools = await db.getAllFromIndex('highSchools', 'name');
    
    if (!highSchools.length) {
      HTTP.get(`${apiUrl}/rest/high_schools`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('highSchools', 'readwrite');
          data.data.forEach(highSchool => {
            tx.store.add({
              id: highSchool.id,
              name: highSchool.name,
              city: highSchool.city
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const highSchoolMajors = await db.getAllFromIndex('highSchoolMajors', 'name');

    if (!highSchoolMajors.length) {
      HTTP.get(`${apiUrl}/rest/high_school_majors`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('highSchoolMajors', 'readwrite');
          data.data.forEach(highSchoolMajor => {
            tx.store.add({
              id: highSchoolMajor.id,
              name: highSchoolMajor.name,
              schoolId: highSchoolMajor.school_id
            });
          })
          await tx.done;
        } else {
          throw new Meteor.Error('Error', err)
        }
      });
    }

    const qualifications = await db.getAllFromIndex('qualifications', 'name');

    if (!qualifications.length) {
      HTTP.get(`${apiUrl}/rest/qualifications`, async function (err, data) {
        if (!err) {
          const tx = db.transaction('qualifications', 'readwrite');
          data.data.forEach(qualification => {
            tx.store.add({
              id: qualification.id,
              name: qualification.name,
              code: qualification.q_id
            });
          })
          await tx.done;
        } else {
          reject(true)
        }
      });
    }
  })()
  String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
  };
});
