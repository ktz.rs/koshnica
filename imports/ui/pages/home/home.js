import './home.html';
import '/lib/typeahead.bundle.min.js';
import { latin2cyrillic } from '/imports/utils.js';
import { ReactiveVar } from 'meteor/reactive-var';
import '../../components/graphs/bar.js';
import '../../components/graphs/pie.js';
import '../../components/graphs/sankey.js';
import '/imports/ui/components/loading/loading.html';
import { json2csv } from 'json-2-csv';
import saveAs from 'file-saver';

const categories = [{
  name: 'Неповратна пресељења из Кикиде у иностранство',
  type: 'sankey',
  chart: 'emigration_from_kikinda_abroad'
}, {
  name: 'Неповратна пресељења из Кикинде унутар земље',
  type: 'sankey',
  chart: 'emigration_from_kikinda'
}, {
  name: 'Пресељења због школовања',
  type: 'sankey',
  chart: 'school_emigration'
}, {
  name: 'Пресељења због посла',
  type: 'bar',
  chart: 'job_emigration'
}, {
  name: 'Повратна пресељења',
  type: 'bar',
  chart: 'returning_cities'
}, {
  name: 'Запослени у иностранству',
  type: 'bar',
  chart: 'abroad_working_cities'
}, {
  name: 'Запослени у земљи',
  type: 'pie',
  chart: 'working_cities'
}, {
  name: 'Подаци о школовању',
  type: 'pie',
  chart: 'school_statistics'
}, {
  name: 'Подаци о занимању',
  type: 'pie',
  chart: 'qualification_statistics'
}, {
  name: 'Језици',
  type: 'pie',
  chart: 'languages'
}];

Template.home.onCreated(function () {
  const _this = this;
  this.chart = new ReactiveVar({
    name: categories[2].type,
    data: {
      chartData(data) {
        _this.chartData.set(data);
      },
      endpoint: categories[2].chart,
      chartName: categories[2].name
    }
  })
  this.chartData = new ReactiveVar(null)
});

Template.home.onRendered(function () {
  const instance = Template.instance();
  var substringMatcher = function (strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;
      var q = latin2cyrillic(q);
      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function (i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });

      cb(matches);
    };
  };

  $('#e-categories').typeahead({
    hint: true,
    highlight: true,
    minLength: 0,
  }, {
      limit: 100,
      name: 'categories',
      source: substringMatcher(categories.map(c => c.name))
    })

  $('#e-categories').bind('typeahead:open', () => {
    instance.chart.set(null)
  })

  $('#e-categories').bind('typeahead:select', (ev, suggestion) => {
    categories.forEach(category => {
      if (category.name === suggestion) {
        instance.chart.set({
          name: category.type,
          data: {
            chartData(data) {
              instance.chartData.set(data);
            },
            endpoint: category.chart,
            chartName: category.name
          }
        })
      }
    })
  });
  $('#e-categories').typeahead('val', 'Пресељења због школовања');
});

Template.home.helpers({
  chart: () => Template.instance().chart.get()
});

Template.home.events({
  'click #e-download': (e, templateInstance) => {
    let data = templateInstance.chartData.get();
    json2csv(data, (err, csv) => {
      if (!err) {
        let blob = new Blob([csv], {
          type: 'text/csv'
        });
        saveAs(blob, 'КТЗ_Кошница_отворени_подаци.csv', {
          autoBOM: true
        });
      }
    });
  }
});
