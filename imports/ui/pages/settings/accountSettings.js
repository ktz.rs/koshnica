import './accountSettings.html';
import { Meteor } from 'meteor/meteor';
import UserSchema from '/imports/api/users/users.js'
import { latin2cyrillic } from '/imports/utils.js';
import { DBNAME, DBVERSION } from '/imports/constants';
import { openDB } from 'idb';
import { ReactiveVar } from 'meteor/reactive-var';
import '/lib/typeahead.bundle.min.js';
import Bloodhound from 'bloodhound-js';

Template.accountSettings.onCreated(async function () {
  this.city = new ReactiveVar('');
  this.workingCity = new ReactiveVar('');
  this.selectedCity = new ReactiveVar(null);
  this.selectedWorkingCity = new ReactiveVar(null);
  this.selectedImage = new ReactiveVar(null)

  const db = await openDB(DBNAME, DBVERSION);
  const cityStore = db.transaction('cities').objectStore('cities');
  const city = cityStore.get(Meteor.user().profile.city);
  const workingCity = cityStore.get(Meteor.user().profile.workingCity)
  const getCities = await Promise.all([city, workingCity])
  if (getCities) {
    this.city.set(getCities[0].name)
    this.workingCity.set(getCities[1].name)
    this.selectedCity.set(getCities[0].id)
    this.selectedWorkingCity.set(getCities[1].id)
  }
});

Template.accountSettings.onRendered(async function () {
  const instance = Template.instance();
  const db = await openDB(DBNAME, DBVERSION)

  const citySearchAction = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    identify: function (obj) { return obj.name; },
    local: await db.getAllFromIndex('cities', 'name')
  });

  const citySearch = (q, sync) => {
    const searchString = latin2cyrillic(q)
    citySearchAction.search(searchString, sync)
  }

  $('#e-city').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
      name: 'cities',
      display: 'name',
      source: citySearch
    })

  let selectedCity = '';
  $('#e-city').bind('typeahead:select', (e, suggestion) => {
    instance.selectedCity.set(suggestion.id);
    selectedCity = suggestion.name;
  })

  $('#e-city').bind('typeahead:close', function () {
    if (selectedCity !== $('#e-city').typeahead('val')) {
      instance.selectedCity.set(null);
      $('#e-city').typeahead('val', '')
    }
  })

  $('#e-workingCity').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
      name: 'cities',
      display: 'name',
      source: citySearch
    })

  let selectedWorkingCity = '';
  $('#e-workingCity').bind('typeahead:select', (e, suggestion) => {
    instance.selectedWorkingCity.set(suggestion.id)
    selectedWorkingCity = suggestion.name;
  })

  $('#e-workingCity').bind('typeahead:close', function () {
    if (selectedWorkingCity !== $('#e-workingCity').typeahead('val')) {
      instance.selectedWorkingCity.set(null)
      $('#e-workingCity').typeahead('val', '')
    }
  })
});

Template.accountSettings.events({
  'submit #e-accountSettings': (event, templateInstance) => {
    event.preventDefault();
    // Get the data
    let form = event.currentTarget;
    let profile = {
      name: latin2cyrillic(form['e-name'].value),
      gender: form['e-gender'].value,
      birth: form['e-birth'].value,
      city: templateInstance.selectedCity.get(),
      workingCity: templateInstance.selectedWorkingCity.get(),
      phone: form['e-phone'].value
    };
    let email = form['e-email'].value;
    let image = templateInstance.selectedImage.get();

    if (email !== '' && email !== Meteor.user().emails[0].address) {
      Meteor.call('changeEmail', { email })
    }

    if (image) {
      if (Meteor.user().profile.picture) {
        Meteor.call('imgur.delete', { deleteHash: Meteor.user().profile.picture.deleteHash }, (err, res) => {
          if (err) {
            throw err;
          }
        });
      }
      Meteor.call('imgur.upload', { image }, (err, res) => {
        if (err) {
          throw err;
        }
        const picture = {
          link: res.data.link,
          deleteHash: res.data.deleteHash
        }
        if (picture.link && picture.deleteHash) {
          Meteor.call('registerMedia', picture)
        }
      });
    }

    Meteor.call('accountSettings', { profile })
  },
  'change #e-uploadImage': (event, templateInstance) => {
    if (event.currentTarget.files && event.currentTarget.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        // Preview the image
        $('#e-avatar').attr('src', e.target.result);
        // Save to reactive var
        templateInstance.selectedImage.set(e.target.result);
      }

      reader.readAsDataURL(event.currentTarget.files[0]);
    }
  },
});

Template.accountSettings.helpers({
  accountSettings() {
    return UserSchema.accountSettings
  },
  city() {
    return Template.instance().city.get()
  },
  workingCity() {
    return Template.instance().workingCity.get()
  },
  emailAddress() {
    return Meteor.user().emails[0].address
  }
});
