import './schoolSettings.html';
import { Meteor } from 'meteor/meteor';
import UserSchema from '/imports/api/users/users.js'
import { latin2cyrillic } from '/imports/utils.js';
import { DBNAME, DBVERSION } from '/imports/constants';
import { openDB } from 'idb';
import { ReactiveVar } from 'meteor/reactive-var';
import '/lib/typeahead.bundle.min.js';
import Bloodhound from 'bloodhound-js';

Template.schoolSettings.onCreated(async function () {
  this.highSchoolCity = new ReactiveVar('');
  this.highSchool = new ReactiveVar('');
  this.highSchoolMajor = new ReactiveVar('');
  this.facultyCity = new ReactiveVar('');
  this.faculty = new ReactiveVar('');
  this.facultyMajor = new ReactiveVar('');
  this.selectedHighSchoolCity = new ReactiveVar(null);
  this.selectedHighSchool = new ReactiveVar(null);
  this.selectedHighSchoolMajor = new ReactiveVar(null);
  this.selectedFacultyCity = new ReactiveVar(null);
  this.selectedFaculty = new ReactiveVar(null);
  this.selectedFacultyMajor = new ReactiveVar(null);

  const db = await openDB(DBNAME, DBVERSION);

  const cityStore = db.transaction('cities').objectStore('cities');
  const highSchoolCity = cityStore.get(Meteor.user().profile.highSchoolCity);
  const facultyCity = cityStore.get(Meteor.user().profile.facultyCity)
  const getCities = await Promise.all([highSchoolCity, facultyCity])
  if (getCities) {
    this.highSchoolCity.set(getCities[0].name)
    this.facultyCity.set(getCities[1].name)
    this.selectedHighSchoolCity.set(getCities[0].id)
    this.selectedFacultyCity.set(getCities[1].id)
  }

  const highSchoolStore = db.transaction('highSchools').objectStore('highSchools');
  const highSchool = await highSchoolStore.get(Meteor.user().profile.highSchool);

  if (highSchool) {
    this.highSchool.set(highSchool.name);
    this.selectedHighSchool.set(highSchool.id);
  }

  const highSchoolMajorStore = db.transaction('highSchoolMajors').objectStore('highSchoolMajors');
  const highSchoolMajor = await highSchoolMajorStore.get(Meteor.user().profile.highSchoolMajor);

  if (highSchoolMajor) {
    this.highSchoolMajor.set(highSchoolMajor.name);
    this.selectedHighSchoolMajor.set(highSchoolMajor.id);
  }

  const facultyStore = db.transaction('faculties').objectStore('faculties');
  const faculty = await facultyStore.get(Meteor.user().profile.faculty);

  if (faculty) {
    this.faculty.set(faculty.name);
    this.selectedFaculty.set(faculty.id);
  }

  const facultyMajorStore = db.transaction('facultyMajors').objectStore('facultyMajors');
  const facultyMajor = await facultyMajorStore.get(Meteor.user().profile.facultyMajor);

  if (facultyMajor) {
    this.facultyMajor.set(facultyMajor.name);
    this.selectedFacultyMajor.set(facultyMajor.id);
  }
});

Template.schoolSettings.onRendered(async function () {
  const instance = Template.instance();
  const db = await openDB(DBNAME, DBVERSION)

  const citySearchAction = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    identify: function (obj) { return obj.name; },
    local: await db.getAllFromIndex('cities', 'name')
  });

  const citySearch = (q, sync) => {
    const searchString = latin2cyrillic(q)
    citySearchAction.search(searchString, sync)
  }

  $('#e-highSchoolCity').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
      name: 'cities',
      display: 'name',
      source: citySearch
    })

  let selectedHighSchoolCity = '';
  $('#e-highSchoolCity').bind('typeahead:select', (e, suggestion) => {
    instance.selectedHighSchoolCity.set(suggestion.id);
    selectedHighSchoolCity = suggestion.name;
  })

  $('#e-highSchoolCity').bind('typeahead:close', function () {
    if (selectedHighSchoolCity !== $('#e-highSchoolCity').typeahead('val')) {
      instance.selectedHighSchoolCity.set(null);
      $('#e-highSchoolCity').typeahead('val', '')
    }
  })

  $('#e-facultyCity').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
      name: 'cities',
      display: 'name',
      source: citySearch
    })

  let selectedFacultyCity = '';
  $('#e-facultyCity').bind('typeahead:select', (e, suggestion) => {
    instance.selectedFacultyCity.set(suggestion.id)
    selectedFacultyCity = suggestion.name;
  })

  $('#e-facultyCity').bind('typeahead:close', function () {
    if (selectedFacultyCity !== $('#e-facultyCity').typeahead('val')) {
      instance.selectedFacultyCity.set(null)
      $('#e-facultyCity').typeahead('val', '')
    }
  })

  const highSchoolAction = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    identify: function (obj) { return obj.name; },
    local: await db.getAllFromIndex('highSchools', 'name')
  });

  const highSchoolSearch = (q, sync) => {
    const searchString = latin2cyrillic(q)
    highSchoolAction.search(searchString, sync)
  }

  $('#e-highSchool').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
      name: 'highSchools',
      display: 'name',
      source: highSchoolSearch
    })

  let selectedHighSchool = '';
  $('#e-highSchool').bind('typeahead:select', async function(e, suggestion) {
    instance.selectedHighSchool.set(suggestion.id);
    selectedHighSchool = suggestion.name;
    const highSchoolId = suggestion.id;
    $('#e-highSchoolMajor').prop('disabled', false);
    const highSchoolMajorSearchAction = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      identify: function (obj) { return obj.name; },
      local: await db.getAllFromIndex('highSchoolMajors', 'schoolId', highSchoolId)
    });

    const highSchoolMajorSearch = (q, sync) => {
      const searchString = latin2cyrillic(q)
      highSchoolMajorSearchAction.search(searchString, sync)
    }

    $('#e-highSchoolMajor').typeahead({
      hint: true,
      highlight: true,
      minLength: 0
    }, {
        name: 'highSchoolMajors',
        display: 'name',
        source: highSchoolMajorSearch
      })

    let selectedHighSchoolMajor = '';
    $('#e-highSchoolMajor').bind('typeahead:select', (e, suggestion) => {
      instance.selectedHighSchoolMajor.set(suggestion.id);
      selectedHighSchoolMajor = suggestion.name;
    })

    $('#e-highSchoolMajor').bind('typeahead:close', function () {
      if (selectedHighSchoolMajor !== $('#e-highSchoolMajor').typeahead('val')) {
        instance.selectedHighSchoolMajor.set(null);
        $('#e-highSchoolMajor').typeahead('val', '')
      }
    })
  })

  $('#e-highSchool').bind('typeahead:close', function () {
    $('#e-highSchoolMajor').typeahead('val', '')
    $('#e-highSchoolMajor').val('')
    if (selectedHighSchool !== $('#e-highSchool').typeahead('val')) {
      instance.selectedHighSchoolCity.set(null);
      $('#e-highSchool').typeahead('val', '')
    }
  })

  const facultySearchAction = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    identify: function (obj) { return obj.name; },
    local: await db.getAllFromIndex('faculties', 'name')
  });

  const facultySearch = (q, sync) => {
    const searchString = latin2cyrillic(q)
    facultySearchAction.search(searchString, sync)
  }

  $('#e-faculty').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
  }, {
      name: 'faculty',
      display: 'name',
      source: facultySearch
    })

  let selectedFaculty = '';
  $('#e-faculty').bind('typeahead:select', async function (ev, suggestion) {
    instance.selectedFaculty.set(suggestion.id);
    selectedFaculty = suggestion.name;
    const facultyId = suggestion.id;
    $('#e-facultyMajor').prop('disabled', false);
    const facultyMajorSearchAction = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      identify: function (obj) { return obj.name; },
      local: await db.getAllFromIndex('facultyMajors', 'facultyId', facultyId)
    });

    const facultyMajorSearch = (q, sync) => {
      const searchString = latin2cyrillic(q)
      facultyMajorSearchAction.search(searchString, sync)
    }

    $('#e-facultyMajor').typeahead({
      hint: true,
      highlight: true,
      minLength: 0
    }, {
        name: 'facultyMajors',
        display: 'name',
        source: facultyMajorSearch
      })
    let selectedFacultyMajor = '';
    $('#e-facultyMajor').bind('typeahead:select', (e, suggestion) => {
      instance.selectedFacultyMajor.set(suggestion.id)
      selectedFacultyMajor = suggestion.name;
    })

    $('#e-facultyMajor').bind('typeahead:close', function () {
      if (selectedFacultyMajor !== $('#e-facultyMajor').typeahead('val')) {
        instance.selectedFacultyMajor.set(null)
        $('#e-facultyMajor').typeahead('val', '')
      }
    })
  });

  $('#e-faculty').bind('typeahead:close', function () {
    $('#e-facultyMajor').typeahead('val', '')
    $('#e-facultyMajor').val('')
    if (selectedFaculty !== $('#e-faculty').typeahead('val')) {
      instance.selectedFaculty.set(null);
      $('#e-faculty').typeahead('val', '')
    }
  })
});

Template.schoolSettings.events({
  'submit #e-schoolSettings': (event, templateInstance) => {
    event.preventDefault();
    // Get the data
    let profile = {
      highSchoolCity: templateInstance.selectedHighSchoolCity.get(),
      highSchool: templateInstance.selectedHighSchool.get(),
      highSchoolMajor: templateInstance.selectedHighSchoolMajor.get(),
      facultyCity: templateInstance.selectedFacultyCity.get(),
      faculty: templateInstance.selectedFaculty.get(),
      facultyMajor: templateInstance.selectedFacultyMajor.get()
    };

    Meteor.call('schoolSettings', { profile })
  },
});

Template.schoolSettings.helpers({
  schoolSettings() {
    return UserSchema.schoolSettings
  },
  highSchoolCity() {
    return Template.instance().highSchoolCity.get()
  },
  highSchool() {
    return Template.instance().highSchool.get()
  },
  highSchoolMajor() {
    return Template.instance().highSchoolMajor.get()
  },
  facultyCity() {
    return Template.instance().facultyCity.get()
  },
  faculty() {
    return Template.instance().faculty.get()
  },
  facultyMajor() {
    return Template.instance().facultyMajor.get()
  },
});
