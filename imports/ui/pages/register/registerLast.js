import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import './registerLast.html';
import UserSchema from '../../../api/users/users';
import { openDB } from 'idb';
import { DBNAME, DBVERSION } from '/imports/constants';
import '/lib/typeahead.bundle.min.js';
import Bloodhound from 'bloodhound-js';
import { latin2cyrillic } from '/imports/utils.js';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

const languages = ['Енглески', 'Немачки', 'Шпански', 'Француски', 'Руски', 'Остало'];

Template.registerLast.onCreated(function () {
    this.phoneNum = new ReactiveVar('');
    this.langs = new ReactiveVar([]);
    this.abroad = new ReactiveVar(false);
    this.selectedQualification = new ReactiveVar(null);
    this.selectedWorkingCity = new ReactiveVar(null);
});

Template.registerLast.onRendered(async function () {
    const instance = Template.instance();
    const db = await openDB(DBNAME, DBVERSION)

    const qualificationSearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name; },
        local: await db.getAllFromIndex('qualifications', 'name')
    });

    const qualificationlSearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        qualificationSearchAction.search(searchString, sync)
    }

    $('#e-qualification').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
    }, {
            name: 'qualifications',
            display: 'name',
            source: qualificationlSearch
        })

    let selectedQualification = '';
    $('#e-qualification').bind('typeahead:select', (e, suggestion) => {
        instance.selectedQualification.set(suggestion.id)
        selectedQualification = suggestion.name;
    })

    $('#e-qualification').bind('typeahead:close', function () {
        if (selectedQualification !== $('#e-qualification').typeahead('val')) {
            instance.selectedQualification.set(null)
            $('#e-qualification').typeahead('val', '')
        }
    })

    const citySearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name; },
        local: await db.getAllFromIndex('cities', 'name')
    });

    const citySearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        citySearchAction.search(searchString, sync)
    }

    $('#e-workingCity').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
            name: 'cities',
            display: 'name',
            source: citySearch
        })

    let selectedWorkingCity = '';
    $('#e-workingCity').bind('typeahead:select', (e, suggestion) => {
        instance.selectedWorkingCity.set(suggestion.id)
        selectedWorkingCity = suggestion.name;
    })

    $('#e-workingCity').bind('typeahead:close', function () {
        if (selectedWorkingCity !== $('#e-workingCity').typeahead('val')) {
            instance.selectedWorkingCity.set(null)
            $('#e-workingCity').typeahead('val', '')
        }
    })

    const languagesSearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: languages
    });

    const languagesSearch = (q, sync) => {
        if (q === '') {
            sync(languages);
        } else {
            const searchString = latin2cyrillic(q)
            languagesSearchAction.search(searchString, sync)
        }
    }

    $('#e-languages').typeahead({
        hint: true,
        highlight: true,
        minLength: 0,
    }, {
            name: 'languages',
            source: languagesSearch
        })

    $('#e-languages').bind('typeahead:select', (ev, suggestion) => {
        let langs = instance.langs.get()
        langs.push(suggestion)
        instance.langs.set(langs)
    });

    $('#e-languages').bind('typeahead:close', (ev) => {
        $('#e-languages').typeahead('val', '')
    });
});

Template.registerLast.events({
    'change #e-abroad': (event, templateInstance) => {
        const checkbox = event.currentTarget;
        templateInstance.abroad.set(checkbox.checked);
    },
    'keydown #e-checkPhone': _.debounce((event, templateInstance) => {
        let number = event.currentTarget.value;
        $(event.currentTarget).removeClass('is-invalid');
        $('.invalid-feedback').empty();

        // Check if the user has typed characters instead of numbers
        if (!number.match(/[A-z]/g)) {
            // Parse the phone number
            const phoneNumber = parsePhoneNumberFromString(number, 'RS');
            // Check if the phone number is valid or if it is a possible number
            if (phoneNumber === undefined || !phoneNumber.isValid() || !phoneNumber.isPossible()) {
                $(event.currentTarget).toggleClass('is-invalid');
                $(event.currentTarget.parentNode).append('<div class="invalid-feedback">Број телефона није валидан!</div>');
            } else {
                templateInstance.phoneNum.set(phoneNumber.getURI());
            }
        } else {
            $(event.currentTarget).toggleClass('is-invalid');
            $(event.currentTarget.parentNode).append('<div class="invalid-feedback">Број телефона није валидан!</div>');
        }
    }, 300),
    'submit #e-registerLast': (event, templateInstance) => {
        event.preventDefault();

        let form = event.currentTarget;
        let profile = {
            qualification: templateInstance.selectedQualification.get(),
            languages: templateInstance.langs.get(),
            phone: form["e-checkPhone"].value
        }

        if (templateInstance.abroad.get()) {
            profile.abroadWorkingCity = latin2cyrillic(form["e-abroadWorkingCity"].value)
        } else {
            profile.workingCity = templateInstance.selectedWorkingCity.get()
        }
        
        if (profile.qualification && (profile.workingCity || profile.abroadWorkingCity) && profile.languages.length && profile.phone) {
            Meteor.call('registerLast', { profile }, (err, data) => {
                if (!err) {
                    Meteor.call('changeFlagOfRegistration', { registerState: 'done' })
                    Meteor.call('saveUserToApi')
                    FlowRouter.go('home');
                }
            })
        }
    }
});

Template.registerLast.helpers({
    lastData: () => UserSchema.lastData,
    selectedLanguages: () => Template.instance().langs.get(),
    abroad: () => Template.instance().abroad.get()
});
