import { ReactiveVar } from 'meteor/reactive-var';
import './registerBiography.html';
import UserSchema from '../../../api/users/users';
import { userDocuments } from '/lib/collections';

Template.registerBiography.onCreated(function() {
    this.biography = new ReactiveVar('');
});

Template.registerBiography.events({
    'change #e-uploadBiography': (event, templateInstance) => {
        if (event.currentTarget.files && event.currentTarget.files[0]) {
            templateInstance.biography.set(event.currentTarget.files[0]);
        }
    },
    'click #e-skipRegistration': (event, templateInstance) => {
        Meteor.call('changeFlagOfRegistration', { registerState: 'registerLast' })
        templateInstance.data.nextRegistrationPage('registerLast');
    },
    'submit #e-registerBiography': (event, templateInstance) => {
        event.preventDefault();
        const upload = userDocuments.insert({
            file: templateInstance.biography.get()
        }, false);

        upload.on('end', function (error) {
            if (error) {
                alert('Error during upload: ' + error);
            } else {
                Meteor.call('changeFlagOfRegistration', { registerState: 'registerLast' })
                templateInstance.data.nextRegistrationPage('registerLast');
            }
        });

        // Start the upload
        upload.start();
    }
});

Template.registerBiography.helpers({
    'registerMedia': () => UserSchema.media
})
