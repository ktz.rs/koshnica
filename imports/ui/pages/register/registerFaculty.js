import UserSchema from '../../../api/users/users';
import { openDB } from 'idb';
import { DBNAME, DBVERSION } from '/imports/constants';
import '/lib/typeahead.bundle.min.js';
import Bloodhound from 'bloodhound-js';
import { latin2cyrillic } from '/imports/utils.js';
import { ReactiveVar } from 'meteor/reactive-var';

import './registerFaculty.html';

Template.registerFaculty.onCreated(function() { 
    this.abroad = new ReactiveVar(false);
    this.selectedFaculty = new ReactiveVar(null);
    this.selectedFacultyCity = new ReactiveVar(null);
    this.selectedFacultyMajor = new ReactiveVar(null);
});

Template.registerFaculty.onRendered(async function () {
    const instance = Template.instance();
    const db = await openDB(DBNAME, DBVERSION)

    const citySearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name; },
        local: await db.getAllFromIndex('cities', 'name')
    });

    const citySearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        citySearchAction.search(searchString, sync)
    }

    $('#e-facultyCity').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'cities',
        display: 'name',
        source: citySearch
    })

    let selectedFacultyCity = '';
    $('#e-facultyCity').bind('typeahead:select', (e, suggestion) => {
        instance.selectedFacultyCity.set(suggestion.id)
        selectedFacultyCity = suggestion.name;
    })

    $('#e-facultyCity').bind('typeahead:close', function () {
        if (selectedFacultyCity !== $('#e-facultyCity').typeahead('val')) {
            instance.selectedFacultyCity.set(null)
            $('#e-facultyCity').typeahead('val', '')
        }
    })

    const facultySearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name; },
        local: await db.getAllFromIndex('faculties', 'name')
    });

    const facultySearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        facultySearchAction.search(searchString, sync)
    }

    $('#e-faculty').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
    }, {
        name: 'faculty',
        display: 'name',
        source: facultySearch
    })

    let selectedFaculty = '';
    $('#e-faculty').bind('typeahead:select', async function (ev, suggestion) {
        instance.selectedFaculty.set(suggestion.id);
        selectedFaculty = suggestion.name;
        const facultyId = suggestion.id;
        $('#e-facultyMajor').prop('disabled', false);
        const facultyMajorSearchAction = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) { return obj.name; },
            local: await db.getAllFromIndex('facultyMajors', 'facultyId', facultyId)
        });

        const facultyMajorSearch = (q, sync) => {
            const searchString = latin2cyrillic(q)
            facultyMajorSearchAction.search(searchString, sync)
        }

        $('#e-facultyMajor').typeahead({
            hint: true,
            highlight: true,
            minLength: 0
        }, {
            name: 'facultyMajors',
            display: 'name',
            source: facultyMajorSearch
        })
        let selectedFacultyMajor = '';
        $('#e-facultyMajor').bind('typeahead:select', (e, suggestion) => {
            instance.selectedFacultyMajor.set(suggestion.id)
            selectedFacultyMajor = suggestion.name;
        })

        $('#e-facultyMajor').bind('typeahead:close', function () {
            if (selectedFacultyMajor !== $('#e-facultyMajor').typeahead('val')) {
                instance.selectedFacultyMajor.set(null)
                $('#e-facultyMajor').typeahead('val', '')
            }
        })
    });

    $('#e-faculty').bind('typeahead:close', function () {
        if (selectedFaculty !== $('#e-faculty').typeahead('val')) {
            instance.selectedFaculty.set(null);
            $('#e-faculty').typeahead('val', '')
        }
    })
});

Template.registerFaculty.events({
    'change #e-abroad': (event, templateInstance) => {
        const checkbox = event.currentTarget;
        templateInstance.abroad.set(checkbox.checked);
    },
    'submit #e-registerFaculty': (event, templateInstance) => {
        event.preventDefault();

        // Get the value
        let form = event.currentTarget;
        let profile = {
            workingProfession: latin2cyrillic(form["e-workingProfession"].value)
        }

        if (templateInstance.abroad.get()) {
            profile.abroadFacultyCity = latin2cyrillic(form["e-abroadFacultyCity"].value)
            profile.abroadFaculty = latin2cyrillic(form["e-abroadFaculty"].value)
            profile.abroadFacultyMajor = latin2cyrillic(form["e-abroadFacultyMajor"].value)
        } else {
            profile.facultyCity = templateInstance.selectedFacultyCity.get();
            profile.faculty = templateInstance.selectedFaculty.get();
            profile.facultyMajor = templateInstance.selectedFacultyMajor.get();
        }

        if ((profile.facultyCity || profile.abroadFacultyCity) && (profile.faculty || profile.abroadFaculty) && (profile.facultyMajor || profile.abroadFacultyMajor) && profile.workingProfession) {
            Meteor.call('registerFaculty', { profile }, (err, data) => {
                if (!err) {
                    Meteor.call('changeFlagOfRegistration', { registerState: 'registerMedia' })
                    templateInstance.data.nextRegistrationPage('registerMedia');
                }
            })
        }
    },
});

Template.registerFaculty.helpers({
    registerFaculty() {
        return UserSchema.faculty;
    },
    abroad: () => Template.instance().abroad.get()
});
