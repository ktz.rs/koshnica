import { ReactiveVar } from 'meteor/reactive-var';
import './registerMedia.html';
import UserSchema from '../../../api/users/users';

Template.registerMedia.onCreated(function() {
    this.image = new ReactiveVar('');
    this.biography = new ReactiveVar('');
});

Template.registerMedia.events({
    'change #e-uploadImage': (event, templateInstance) => {
        if (event.currentTarget.files && event.currentTarget.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                // Preview the image
                $('#imagePreview').attr('src', e.target.result);
                // Save to reactive var
                templateInstance.image.set(e.target.result);
            }

            reader.readAsDataURL(event.currentTarget.files[0]);
        }
    },
    'click #e-skipRegistration': (event, templateInstance) => {
        Meteor.call('changeFlagOfRegistration', { registerState: 'registerBiography' })
        templateInstance.data.nextRegistrationPage('registerBiography');
    },
    'submit #e-registerMedia': (event, templateInstance) => {
        event.preventDefault();

        // Upload user image
        Meteor.call('imgur.upload', { image: templateInstance.image.get() }, (err, res) => {
            if (err) {
                throw err;
            }
            const picture = {
                link: res.data.link,
                deleteHash: res.data.deleteHash
            }
            if (picture.link && picture.deleteHash) {
                Meteor.call('registerMedia', picture, (err, data) => {
                    if (!err) {
                        Meteor.call('changeFlagOfRegistration', { registerState: 'registerBiography' })
                        templateInstance.data.nextRegistrationPage('registerBiography');
                    }
                })
            }
        });
    }
});

Template.registerMedia.helpers({
    'registerMedia': () => UserSchema.media
})
