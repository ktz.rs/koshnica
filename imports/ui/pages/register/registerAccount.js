import { Meteor } from 'meteor/meteor';
import './registerAccount.html';
import UserSchema from '/imports/api/users/users.js'
import { Accounts } from 'meteor/accounts-base';
import { latin2cyrillic } from '/imports/utils.js';

Template.registerAccount.events({
    'focus #email': (event) => {
        if ('.invalid-feedback') {
            $(event.currentTarget).removeClass('is-invalid');
            $('.invalid-feedback').remove();
        }
    },
    'submit #e-createAccount': (event, templateInstance) => {
        event.preventDefault();
        // Get the data
        let form = event.currentTarget;
        let profile = {
            name: latin2cyrillic(form.name.value),
            gender: form.gender.value,
            birth: form.date.value
        };
        let password = form.password.value;
        let email = form.email.value;

        Accounts.createUser({ email, password, profile }, (err, data) => {
            if (!err) {
                Meteor.call('changeFlagOfRegistration', { registerState: 'registerPlaceAndSchool' })
                templateInstance.data.nextRegistrationPage('registerPlaceAndSchool');
            }
        })
    }
});

Template.registerAccount.helpers({
    createAccount() {
        return UserSchema.general
    }
});
