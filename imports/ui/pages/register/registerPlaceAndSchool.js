import './registerPlaceAndSchool.html';
import UserSchema from '../../../api/users/users';
import { openDB } from 'idb';
import { DBNAME, DBVERSION } from '/imports/constants';
import '/lib/typeahead.bundle.min.js';
import Bloodhound from 'bloodhound-js';
import { latin2cyrillic } from '/imports/utils.js';
import { ReactiveVar } from 'meteor/reactive-var';

Template.registerPlaceAndSchool.onCreated(function() { 
    this.abroad = new ReactiveVar(false);
    this.selectedCity = new ReactiveVar(null);
    this.selectedPrimarySchoolCity = new ReactiveVar(null);
    this.selectedHighSchoolCity = new ReactiveVar(null);
    this.selectedHighSchool = new ReactiveVar(null);
    this.selectedHighSchoolMajor = new ReactiveVar(null);
});

Template.registerPlaceAndSchool.onRendered(async function () {
    const instance = Template.instance();
    const db = await openDB(DBNAME, DBVERSION)

    const citySearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name; },
        local: await db.getAllFromIndex('cities', 'name')
    });

    const citySearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        citySearchAction.search(searchString, sync)
    }
    
    $('#e-city').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'cities',
        display: 'name',
        source: citySearch
    })
    
    let selectedCity = '';
    $('#e-city').bind('typeahead:select', (e, suggestion) => {
        instance.selectedCity.set(suggestion.id);
        selectedCity = suggestion.name;
    })

    $('#e-city').bind('typeahead:close', function() {
        if (selectedCity !== $('#e-city').typeahead('val')) {
            instance.selectedCity.set(null);
            $('#e-city').typeahead('val', '')
        }
    })

    $('#e-primarySchoolCity').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'cities',
        display: 'name',
        source: citySearch
    })

    let selectedPrimarySchoolCity = '';
    $('#e-primarySchoolCity').bind('typeahead:select', (e, suggestion) => {
        instance.selectedPrimarySchoolCity.set(suggestion.id);
        selectedPrimarySchoolCity = suggestion.name;
    })

    $('#e-primarySchoolCity').bind('typeahead:close', function() {
        if (selectedPrimarySchoolCity !== $('#e-primarySchoolCity').typeahead('val')) {
            instance.selectedPrimarySchoolCity.set(null);
            $('#e-primarySchoolCity').typeahead('val', '')
        }
    })

    $('#e-highSchoolCity').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'cities',
        display: 'name',
        source: citySearch
    })

    let selectedHighSchoolCity = '';
    $('#e-highSchoolCity').bind('typeahead:select', (e, suggestion) => {
        instance.selectedHighSchoolCity.set(suggestion.id);
        selectedHighSchoolCity = suggestion.name;
    })

    $('#e-highSchoolCity').bind('typeahead:close', function() {
        if (selectedHighSchoolCity !== $('#e-highSchoolCity').typeahead('val')) {
            instance.selectedHighSchoolCity.set(null);
            $('#e-highSchoolCity').typeahead('val', '')
        }
    })

    const highSchoolSearchAction = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        identify: function (obj) { return obj.name },
        local: await db.getAllFromIndex('highSchools', 'name')
    });

    const highSchoolSearch = (q, sync) => {
        const searchString = latin2cyrillic(q)
        highSchoolSearchAction.search(searchString, sync)
    }

    $('#e-highSchool').typeahead({
        hint: true,
        highlight: true,
        minLength: 1,
    }, {
        name: 'highSchools',
        display: 'name',
        source: highSchoolSearch
    })

    let selectedHighSchool = ''
    $('#e-highSchool').bind('typeahead:select', async function(ev, suggestion) {
        instance.selectedHighSchool.set(suggestion.id);
        selectedHighSchool = suggestion.name;
        const highSchoolId = suggestion.id;
        $('#e-highSchoolMajor').prop('disabled', false);
        const highSchoolMajorSearchAction = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            identify: function (obj) { return obj.name; },
            local: await db.getAllFromIndex('highSchoolMajors', 'schoolId', highSchoolId)
        });
        
        const highSchoolMajorSearch = (q, sync) => {
            const searchString = latin2cyrillic(q)
            highSchoolMajorSearchAction.search(searchString, sync)
        }

        $('#e-highSchoolMajor').typeahead({
            hint: true,
            highlight: true,
            minLength: 0
        }, {
            name: 'highSchoolMajors',
            display: 'name',
            source: highSchoolMajorSearch
        })

        let selectedHighSchoolMajor = '';
        $('#e-highSchoolMajor').bind('typeahead:select', (e, suggestion) => {
            instance.selectedHighSchoolMajor.set(suggestion.id);
            selectedHighSchoolMajor = suggestion.name;
        })

        $('#e-highSchoolMajor').bind('typeahead:close', function() {
            if (selectedHighSchoolMajor !== $('#e-highSchoolMajor').typeahead('val')) {
                instance.selectedHighSchoolMajor.set(null);
                $('#e-highSchoolMajor').typeahead('val', '')
            }
        })
    });

    $('#e-highSchool').bind('typeahead:close', function() {
        if (selectedHighSchool !== $('#e-highSchool').typeahead('val')) {
            instance.selectedHighSchool.set(null);
            $('#e-highSchool').typeahead('val', '')
        }
    })
});

Template.registerPlaceAndSchool.events({
    'change #e-abroad': (event, templateInstance) => {
        const checkbox = event.currentTarget;
        templateInstance.abroad.set(checkbox.checked);
    },
    'submit #e-registerPlaceAndSchool': (event, templateInstance) => {
        event.preventDefault();

        // Get the value
        let form = event.currentTarget;
        let profile = {
            primarySchoolCity: templateInstance.selectedPrimarySchoolCity.get(),
            highSchoolCity: templateInstance.selectedHighSchoolCity.get(),
            highSchool: templateInstance.selectedHighSchool.get(),
            highSchoolMajor: templateInstance.selectedHighSchoolMajor.get()
        }

        if (templateInstance.abroad.get()) {
            profile.abroadCity = latin2cyrillic(form["e-abroadCity"].value)
        } else {
            profile.city = templateInstance.selectedCity.get()
        }

        if ((profile.city || profile.abroadCity) && profile.primarySchoolCity && profile.highSchoolCity && profile.highSchool && profile.highSchoolMajor) {
            Meteor.call('registerPlaceAndSchool', { profile }, (err, data) => {
                if (!err) {
                    Meteor.call('changeFlagOfRegistration', { registerState: 'registerFaculty' })
                    templateInstance.data.nextRegistrationPage('registerFaculty');
                }
            })
        }

    }
});

Template.registerPlaceAndSchool.helpers({
    registerPlaceAndSchool() {
        return UserSchema.placeAndSchool;
    },
    abroad: () => Template.instance().abroad.get()
});
