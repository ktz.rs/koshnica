import { ReactiveVar } from 'meteor/reactive-var';

import './register.html';

Template.register.onCreated(function() { 
    this.registerTemplate = new ReactiveVar('registerAccount');
    if (this.data.data) {
        this.registerTemplate.set(this.data.data);
    }
});

Template.register.helpers({
    'registerAccount': function(){
        return Template.instance().registerTemplate.get();
    },
    'changeRegistrationState': function() {
        const instance = Template.instance();
        return {
            nextRegistrationPage(page) {
                instance.registerTemplate.set(page);
            }
        }
    }
});
