import { Accounts } from 'meteor/accounts-base';
import './reset.html';

Template.reset.events({
    'submit form': (event) => {
        event.preventDefault();

        // Send the e-mail
        Accounts.forgotPassword({
            email: event.currentTarget.email.value
        }, (err) => {
            if (err) {
                // Hindu all the way!
                $('main').append('<div class="modal-backdrop fade show">');
                $('.modal.fade').addClass('show').attr('style', 'padding-right: 15px; display: block;');
                $('.modal-body').empty().append(`<p class="text-center">Унета е-пошта не постоји у нашој бази.</p>`);
            }
        }); 
    }
});