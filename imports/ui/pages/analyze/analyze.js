import { Meteor } from 'meteor/meteor';
import echarts from 'echarts';
import converter from 'json-2-csv';
import './analyze.html';

Template.analyze.events({
    'change #e-uploadCSV': (event) => {
        // Get the file data
        let csv = event.currentTarget.files[0];
        // Parse the file
        let reader = new FileReader();

        if (csv) {
            reader.readAsText(csv);
        }

        reader.addEventListener('load', () => {
            let csvData = reader.result;
            converter.csv2json(csvData, (err, csv) => {
                if (err) {
                    throw err;
                }
                $('.analyze-sum').html(`<p>Укупан број регистрованих чланова: <span id="analyze-sum-num">${csv.length}</span></p>`);
                createGenderChart(csv);
                createCityChart(csv);
            });
        });
    },
    'click #e-loadDB': () => {
        Meteor.call('analyzeData', (err, res) => {
            if (!err) {
                $('.analyze-sum').html(`<p>Укупан број регистрованих чланова: <span id="analyze-sum-num">${res.length}</span></p>`);
                createGenderChart(res);
                createCityChart(res);
            } else {
                console.error(err);
            }
        });
    }
});

function createGenderChart(csv) {
    // Initialize charts
    let chart = echarts.init(document.querySelector('#analyze-data-gender'));

    let maleNum = 0;
    let femaleNum = 0;
    csv.map((values) => {
        if (values.gender === 'Мушко') {
            maleNum++;
        } else {
            femaleNum++;
        }
    });

    // Add options
    var options = {
        title: {
            text: 'Број регистрованих по половима'
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        series: [{
            name: 'Пол',
            type: 'pie',
            radius: '70%',
            color: ['#3f51b5', '#e91e63'],
            avoidLabelOverlap: false,
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },
            data: [{
                    value: maleNum,
                    name: 'Mушко'
                },
                {
                    value: femaleNum,
                    name: 'Женско'
                }
            ]
        }]
    };
    // Draw the chart
    chart.setOption(options);
}

function createCityChart(csv) {
    // Initialize charts
    let chart = echarts.init(document.querySelector('#analyze-data-city'));

    function iterateCities() {
        let arr = [];
        let hist = {};
        // Put every city in one array
        let cityName = csv.map((name) => name.city);
        // Now do some magic => example create: " { 'Кикинда': 100 } "
        cityName.map( function (a) { if (a in hist) hist[a] ++; else hist[a] = 1; } );
        // Get the object keys and values
        for (let [name, value] of Object.entries(hist)) {
            // Put the object in array
            // e.g. [{value: 100, name: 'Кикинда'}, {value: 50, name: 'Нови Сад'},...] 
            arr.push({value, name});
        }
        // Return array of objects
        return arr;
    }
    // Add options
    let options = {
        title: {
            text: 'Место школовања'
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        series: [{
            name: 'Град',
            type: 'pie',
            radius: '70%',
            data: iterateCities(),
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }]
    }
    // Draw the chart
    chart.setOption(options);
}