import { ReactiveVar } from 'meteor/reactive-var';
import { userDocuments } from '/lib/collections';
import converter from 'json-2-csv';
import { saveAs } from 'file-saver';
import './dashboard.html';

// Subscribe to users collection
Template.dashboard.onCreated(function () {
    this.users = new ReactiveVar();
    this.users.set(this.data.users);
});

Template.dashboard.events({
    'click #e-exportCSV': () => {
        Meteor.call('exportCSV', (err, users) => {
            if (!err) {
                converter.json2csv(users, (err, csv) => {
                    if (!err) {
                        let blob = new Blob([csv], { type: 'text/csv' })
                        saveAs(blob, 'КТЗ_База.csv', { autoBOM: true });
                    } else {
                        console.error(err);
                    }
                })
            } else {
                console.error(err);
            }
        });
    }
});

// Render the data onto the table
Template.dashboard.helpers({
    userData() {
        return Template.instance().users.get();
    },
    userDocument(userId) {
        let file = userDocuments.findOne({ userId });
        return Spacebars.SafeString(`
        <a href="${file.link()}" download="${file.name}" target="_parent">
            ${file.name}
        </a>`
        );
    },
    userBirth(date) {
        const cccc = new Intl.DateTimeFormat('SR', {
            day: 'numeric',
            month: 'long',
            year: 'numeric'
        }).format(new Date(date));
        return cccc;
    }
});
