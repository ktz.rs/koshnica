import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import SimpleSchema from 'simpl-schema';
import './signIn.html';

Template.signIn.events({
    'submit #e-submit': (event) => {
        event.preventDefault();

        // Get the values
        let form = event.currentTarget;
        let email = form.email.value;
        let password = form.password.value;

        // Login
        Meteor.loginWithPassword(email, password, (err) => {
            if (!err) {
                FlowRouter.go('home');
            } else {
                if (err.reason === 'User not found') {
                    showModal('Непостојећи налог.');
                } else if (err.reason === 'Incorrect password') {
                    showModal('Неисправна лозинка.');
                }
            }
        });

        function showModal(text) {
            // Hindu all the way!
            $('main').append('<div class="modal-backdrop fade show">');
            $('.modal.fade').addClass('show').attr('style', 'padding-right: 15px; display: block;');
            $('.modal-body').empty().append(`<p class="text-center">${text}</p>`);
        }
    },
    'click #e-signFacebook': () => {
        Meteor.loginWithFacebook({ requestPermissions: ['email'] }, (err) => {
            if (!err) {
                FlowRouter.go('register');
            } else {
                console.error(err);
            }
        })
    },
    'click #e-signLinkedIn': () => {
        Meteor.loginWithLinkedIn({ requestPermissions: ['r_emailaddress', 'r_liteprofile'] }, (err) => {
            if (!err) {
                FlowRouter.go('register');
            } else {
                console.error(err);
            }
        })
    },
});

Template.signIn.helpers({
    loginForm() {
        return new SimpleSchema({
            email: {
                type: String,
                label: "Е-пошта",
                max: 50
            },
            password: {
                type: String,
                label: "Лозинка",
                min: 6
            }
        });
    }
});
