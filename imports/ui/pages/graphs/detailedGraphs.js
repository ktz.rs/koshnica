import './detailedGraphs.html';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { ReactiveVar } from 'meteor/reactive-var';
import '../../components/graphs/bar.js';
import '../../components/graphs/line.js';
import '../../components/graphs/pie.js';

Template.detailedGraphs.onCreated(function() { 
  const { queryParams } = FlowRouter.current();
  this.chart = new ReactiveVar({ name: queryParams.type, data: {
    endpoint: queryParams.chart,
    chartName: queryParams.chartName
  }})
});

Template.detailedGraphs.helpers({
  chart: () => Template.instance().chart.get(),
  isActive: (endpoint) => {
    if (FlowRouter.current().queryParams.chart === endpoint) {
      return 'active'
    }
    return;
  }
});


Template.detailedGraphs.onRendered(function() {
  const activeElements = document.getElementsByClassName('active')

  Array.from(activeElements).forEach(el => {
    el.parentElement.parentElement.open = true;
  })
});
