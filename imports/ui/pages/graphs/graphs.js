import { Meteor } from 'meteor/meteor';
import { json2csv } from 'json-2-csv';
import { ReactiveVar } from 'meteor/reactive-var';
import saveAs from 'file-saver';
import './graphs.html';
import '../../components/graphs/bar.js';
import '../../components/graphs/line.js';
import '../../components/graphs/pie.js';

const chartData = {
    languages: {
        endpoint: 'languages',
        chartName: 'Језици'
    },
    schoolEmigration: {
        endpoint: 'school_emigration'
    },
    jobEmigration: {
        endpoint: 'job_emigration'
    },
    workingPlace: {
        endpoint: 'koshnica_users?working_city=is.null',
        chartName: 'Место запослености',
        firstDataName: 'Запослено у иностранству',
        lastDataName: 'Запослено у земљи'
    }
}

let apiUrl = 'https://kosnica.ktz.rs';

Template.graphs.onCreated(function () {
    this.kikindaEmigrationSchool = new ReactiveVar(0)
    this.kikindaEmigrationJob = new ReactiveVar(0);
    this.kikindaAbroadEmigration = new ReactiveVar(0)
    this.kikindaImigration = new ReactiveVar(0);
});

Template.graphs.events({
    'click #e-anonymizeData': () => {
        Meteor.call('exportAnoData', (err, data) => {
            if (!err) {
                json2csv(data, (err, csv) => {
                    if (!err) {
                        let blob = new Blob([csv], {
                            type: 'text/csv'
                        });
                        saveAs(blob, 'КТЗ_Кошница_отворени_подаци.csv', {
                            autoBOM: true
                        });
                    }
                });
            }
        });
    }
});

Template.graphs.helpers({
    'kikindaEmigrationSchool': () => Template.instance().kikindaEmigrationSchool.get(),
    'kikindaEmigrationJob': () => Template.instance().kikindaEmigrationJob.get(),
    'kikindaAbroadEmigration': () => Template.instance().kikindaAbroadEmigration.get(),
    'kikindaImigration': () => Template.instance().kikindaImigration.get(),
    'chartData': () => chartData
});

Template.graphs.onRendered(async function () {
    const instance = Template.instance();
    if (Meteor.isDevelopment) {
        apiUrl = 'http://localhost:5000'
    }

    const kikindaEmigrationSchool = 'koshnica_users?or=(and(primary_school_city.eq.491,high_school_city.neq.491),and(high_school_city.eq.491,faculty_city.neq.491),and(high_school_city.eq.491,faculty_city.is.null))'
    HTTP.get(`${apiUrl}/rest/${kikindaEmigrationSchool}`, async function (err, data) {
        if (!err) {
            instance.kikindaEmigrationSchool.set(data.data.length)
        }
    });

    const kikindaEmigrationJob = 'koshnica_users?or=(and(working_city.neq.491,primary_school_city.eq.491),and(working_city.is.null,primary_school_city.eq.491))'
    HTTP.get(`${apiUrl}/rest/${kikindaEmigrationJob}`, async function (err, data) {
        if (!err) {
            instance.kikindaEmigrationJob.set(data.data.length)
        }
    });

    const kikindaAbroadEmigration = 'koshnica_users?and=(primary_school_city.eq.491,city.is.null)'
    HTTP.get(`${apiUrl}/rest/${kikindaAbroadEmigration}`, async function (err, data) {
        if (!err) {
            instance.kikindaAbroadEmigration.set(data.data.length)
        }
    });

    const kikindaImigration = 'koshnica_users?and=(city.eq.491,or(high_school_city.is.null,faculty_city.is.null))'
    HTTP.get(`${apiUrl}/rest/${kikindaImigration}`, async function (err, data) {
        if (!err) {
            instance.kikindaImigration.set(data.data.length)
        }
    });

})
