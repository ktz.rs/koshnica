import './profile.html';
import '../settings/accountSettings';
import '../settings/schoolSettings';

Template.profile.onCreated(function onCreated() {
  this.selectedSettings = new ReactiveVar('accountSettings');
});

Template.profile.events({
  'click #e-changeSettings': (event, templateInstance) => {
    templateInstance.selectedSettings.set(event.currentTarget.dataset.settings);
  }
});

Template.profile.helpers({
  selectedSettings: () => Template.instance().selectedSettings.get(),
  activeMenu(menu) {
    if (menu === Template.instance().selectedSettings.get()) {
      return 'active'
    }
  }
})
