import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import './layout.html';
import '/imports/ui/components/header/header.html';
import '/imports/ui/components/footer/footer.html';

Template.layout.events({
    'click #e-signOut': () => {
        // Logout
        Meteor.logout();
        // Navigate home
        FlowRouter.go('home');
    }
});
