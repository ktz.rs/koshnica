import './modal.html';

Template.modal.events({
    'click #e-modalClose': () => {
        removeModal();
    },
    'click #e-modalOk': () => {
        removeModal();
    }
});

function removeModal() {
    $('body').removeClass('modal-open').removeAttr('style');
    $('.modal-backdrop').remove();
    $('.modal').removeAttr('style');
}