import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import './tos.html';

Template.tos.onRendered(() => {
    // Check if the user has already accepted the ToS
    if (localStorage.getItem('ToS_ACCEPTED')) {
        removeModal();
    }
});

Template.tos.events({
    'click #e-closeModal': (event) => {
        event.preventDefault();
        // Remove modal
        removeModal();
        // Navigate to home page
        FlowRouter.go('home');
    },
    'click #e-modalAccept': () => {
        // Remove modal
        removeModal();
        // ToS Accepted
        localStorage.setItem('ToS_ACCEPTED', new Date());
    },
    'click #e-modalRefuse': () => {
        // Remove modal
        removeModal();
        // Navigate to home page
        FlowRouter.go('home');
    }
});

function removeModal() {
    $('body').removeClass('modal-open').removeAttr('style');
    $('.modal-backdrop').remove();
    $('.modal').removeAttr('style');
}