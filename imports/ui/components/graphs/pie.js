import './pie.html';
import echarts from 'echarts';
import '/imports/ui/themes/roma.js';

let apiUrl = 'https://kosnica.ktz.rs';

Template.pie.onRendered(async function () {
  if (Meteor.isDevelopment) {
    apiUrl = 'http://localhost:5000'
  }
  const { endpoint, chartName, chartData } = this.data;
    
  HTTP.get(`${apiUrl}/rest/${endpoint}`, async function (err, data) {
    if (!err) {
      let dataSet = data.data;
      if (chartData) {
        chartData(dataSet)
      }
      if (chartName == 'Место запослености') {
        const userPromise = new Promise((resolve, reject) => {
          HTTP.get(`${apiUrl}/rest/koshnica_users`, async function (err, data) {
            if (!err) {
              resolve(data.data.length)
            }
          });
        })

        const numberOfUsers = await userPromise;

        const dataNumber = data.data.length;
        dataSet = [
          { value: dataNumber, name: 'Запослено у иностранству' },
          { value: numberOfUsers, name: 'Запослено у земљи' },
        ]
      }

      let myChart = echarts.init(document.getElementById(endpoint), 'roma');
      // specify chart configuration item and data
      let option = {
        title: {
            text: chartName,
            left: 'center'
        },
        tooltip: {
          trigger: 'item',
          formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        series: [
          {
            name: chartName,
            type: 'pie',
            radius: ['20%', '70%'],
            avoidLabelOverlap: false,
            label: {
              normal: {
                show: false,
                position: 'center'
              },
            },
            data: dataSet
          }
        ]
      };

      myChart.setOption(option);
    }
  });

});
