import './line.html';
import echarts from 'echarts';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import '/imports/ui/themes/roma.js';

let apiUrl = 'https://kosnica.ktz.rs';

Template.line.onRendered(function () {
  const { endpoint, chartData } = this.data;
  if (Meteor.isDevelopment) {
    apiUrl = 'http://localhost:5000'
  }
  HTTP.get(`${apiUrl}/rest/${endpoint}`, async function (err, data) {
    if (!err) {
      let myChart = echarts.init(document.getElementById(endpoint), 'roma');
      if (chartData) {
        chartData(data.data)
      }
      let dataSplit = data.data.map(item => item.name)
      if (FlowRouter.current().route.name !== 'detailedGraphs') {
        dataSplit = data.data.map(item => item.name.split('').splice(0, 2).join('') + '...')
      }
      // specify chart configuration item and data
      let option = {
        xAxis: {
          type: 'category',
          boundaryGap: false,
          data: dataSplit
        },
        yAxis: {},
        series: [{
          data: data.data.map(item => item.value),
          type: 'line',
          areaStyle: {},
          smooth: true
        }]
      };
      myChart.setOption(option);
    }
  });
});
