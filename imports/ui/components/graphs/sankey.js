import './sankey.html';
import echarts from 'echarts';
import '/imports/ui/themes/roma.js';
import { _ } from 'meteor/underscore'

let apiUrl = 'https://kosnica.ktz.rs';

Template.sankey.onRendered(function () {
  const { endpoint, chartName, chartData } = this.data;
  if (Meteor.isDevelopment) {
      apiUrl = 'http://localhost:5000'
  }
  HTTP.get(`${apiUrl}/rest/${endpoint}`, async function (err, data) {
    if (!err) {
      let myChart = echarts.init(document.getElementById(endpoint), 'roma');
      if (chartData) {
        chartData(data.data)
      }
      // specify chart configuration item and data
      const arr = {
        links: data.data.map(item => {
          return [{
            source: "Кикинда",
            target: item.name,
            value: 1,
          }, {
            source: item.name,
            target: item.value + "",
            value: 1
          }]
        }).reverse().reduce((acc, val) => acc.concat(val)),
        nodes: data.data.map(item => {
          return {
            name: item.name
          }
        }).concat(data.data.map((item, index, array) => {
          return {
            name: item.value + ""
          }
        })).reverse()
      }

      
      arr.nodes.push({ name: "Кикинда" })

      let option = {
        legend: {
            data:['Број људи']
        },
        title: {
          text: chartName,
          left: 'left'
        },
        series: [
          {
              top: "20%",
              name: 'Број људи',
              type: 'sankey',
              data: _.uniq(arr.nodes, function(x){
                return x.name;
              }),
              links: arr.links,
              focusNodeAdjacency: true,
              levels: [{
                  depth: 0,
                  itemStyle: {
                      color: '#fbb4ae'
                  },
                  lineStyle: {
                      color: 'source',
                      opacity: 0.6
                  }
              }, {
                  depth: 1,
                  itemStyle: {
                      color: '#b3cde3'
                  },
                  lineStyle: {
                      color: 'source',
                      opacity: 0.6
                  }
              }, {
                  depth: 2,
                  itemStyle: {
                      color: '#ccebc5'
                  },
                  lineStyle: {
                      color: 'source',
                      opacity: 0.6
                  }
              }],
              lineStyle: {
                  normal: {
                      curveness: 0.5
                  }
              }
          }
        ]
      };


      myChart.setOption(option);
    }
  });
});
