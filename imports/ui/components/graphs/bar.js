import './bar.html';
import echarts from 'echarts';
import '/imports/ui/themes/roma.js';

let apiUrl = 'https://kosnica.ktz.rs';

Template.bar.onRendered(function () {
  const { endpoint, chartName, chartData } = this.data;
  if (Meteor.isDevelopment) {
      apiUrl = 'http://localhost:5000'
  }
  HTTP.get(`${apiUrl}/rest/${endpoint}`, async function (err, data) {
    if (!err) {
      let myChart = echarts.init(document.getElementById(endpoint), 'roma');
      if (chartData) {
        chartData(data.data)
      }
      // specify chart configuration item and data
      const arr = data.data.map(item => [item.value, item.name]).reverse()
      let option = {
        title: {
          text: chartName,
          left: 'left'
        },
        legend: {
            data:['Број људи']
        },
        dataset: {
          source: [['amount', 'product'], ...arr]
        },
        grid: { containLabel: true },
        xAxis: {},
        yAxis: { type: 'category' },
        series: [
          {
            name: 'Број људи',
            type: 'bar',
            encode: {
              // Map the "amount" column to X axis.
              x: 'amount',
              // Map the "product" column to Y axis
              y: 'product'
            }
          }
        ]
      };


      myChart.setOption(option);
    }
  });
});
