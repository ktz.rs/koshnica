import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { Imgur } from 'meteor/simple:imgur';
import Future from 'fibers/future';
import { _ } from 'meteor/underscore';

const ImgurMethods = {
    uploadImage: new ValidatedMethod({
        name: 'imgur.upload',
        validate: new SimpleSchema({
            image: { type: String },
        }).validator(),
        run({ image }) {
            const future = new Future();
            Imgur.upload({
                image: image,
                apiKey: Meteor.settings.public.Imgur.appID
            }, (err, data) => {
                if (err) {
                    console.error(err);
                    future.return({
                        success: false,
                        data: err.reason,
                    });
                } else {
                    const link = data.link.replace('http://', 'https://');
                    const image = {
                        link,
                        deleteHash: data.deletehash,
                    };
                    future.return({
                        success: true,
                        data: image,
                    });
                }
            });
            return future.wait();
        },
    }),
    deleteImage: new ValidatedMethod({
        name: 'imgur.delete',
        validate: new SimpleSchema({
            deleteHash: { type: String },
        }).validator(),
        run({ deleteHash }) {
            const future = new Future();
            Imgur.delete({
                deleteHash,
                apiKey: Meteor.settings.public.Imgur.appID
            }, (err, data) => {
                if (err) {
                    future.return({
                        success: false,
                        data: err,
                    });
                } else {
                    future.return({
                        success: true,
                        data,
                    });
                }
            });
            return future.wait();
        },
    }),
};

// Get list of all method names on Imgur
const IMGUR_METHODS = _.pluck([
    ImgurMethods.uploadImage,
    ImgurMethods.deleteImage,
], 'name');

DDPRateLimiter.addRule({
    name(name) {
        return _.contains(IMGUR_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
}, 5, 1000);

export default ImgurMethods
