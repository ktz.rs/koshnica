import { ReactiveVar } from 'meteor/reactive-var';
import './header.html';
import { openDB } from 'idb';
import { DBNAME, DBVERSION } from '/imports/constants';
import saveAs from 'file-saver';

Template.header.onCreated(function() { 
    this.bubbleOpened = new ReactiveVar(false);
});

Template.header.events({ 
    'click #e-signOut': function() {
        Meteor.logout();
    },
    'click #e-openBubble': function(event, templateInstance) {
        templateInstance.bubbleOpened.set(!templateInstance.bubbleOpened.get())
    },
    'click #e-downloadData': async function(e, templateInstance) {
        const db = await openDB(DBNAME, DBVERSION)
        const cities = await db.getAllFromIndex('cities', 'name');
        const faculties = await db.getAllFromIndex('faculties', 'name');
        const facultyMajors = await db.getAllFromIndex('facultyMajors', 'name');
        const universities = await db.getAllFromIndex('universities', 'name');
        const highSchools = await db.getAllFromIndex('highSchools', 'name');
        const highSchoolMajors = await db.getAllFromIndex('highSchoolMajors', 'name');
        const qualifications = await db.getAllFromIndex('qualifications', 'name');

        const data = {
            cities,
            faculties,
            facultyMajors,
            universities,
            highSchools,
            highSchoolMajors,
            qualifications
        }

        let blob = new Blob([JSON.stringify(data)], {
          type: 'application/json'
        });
        saveAs(blob, 'КТЗ_Кошница_отворени_подаци.json', {
          autoBOM: true
        });
      }
});

Template.header.helpers({
    userRegistrationCompleted() {
        return Meteor.userId() && (Meteor.users.findOne({ _id: Meteor.userId() }).flags.registerState === 'done');
    },
    bubbleOpened: () => Template.instance().bubbleOpened.get()
});
