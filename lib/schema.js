import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

let userSchema = new SimpleSchema({
    services: {
        type: Object,
        blackbox: true
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Object
    },
    'profile.name': {
        type: String,
        min: 10,
        max: 50
    },
    'profile.picture': {
        type: Object
    },
    'profile.picture.link': {
        type: String
    },
    'profile.picture.deleteHash': {
        type: String,
        optional: true
    },
    'profile.gender': {
        type: String,
        min: 5,
        max: 6
    },
    'profile.birth': {
        type: Date
    },
    'profile.city': {
        type: String,
        min: 2,
        max: 20
    },
    'profile.phone': {
        type: String,
        min: 10,
        max: 17
    },
    school: {
        type: Object
    },
    'school.high': {
        type: Object
    },
    'school.high.city': {
        type: String,
    },
    'school.high.name': {
        type: String
    },
    'school.high.class': {
        type: String
    },
    'school.faculty': {
        type: Object
    },
    'school.faculty.city': {
        type: String
    },
    'school.faculty.name': {
        type: String
    },
    'school.faculty.class': {
        type: String
    },
    occupation: {
        type: Object
    },
    'occupation.employed': {
        type: Boolean
    },
    'occupation.name': {
        type: String
    },
    'occupation.city': {
        type: String,
        optional: true
    },
    languages: {
        type: Array
    },
    'languages.$': {
        type: String
    },
    tos: {
        type: Object
    },
    'tos.accepted': {
        type: Boolean
    },
    'tos.date': {
        type: Date
    },
    roles: {
        type: Array,
        optional: true
    },
    'roles.$': {
        type: String,
        optional: true
    },
    emails: {
        type: Array
    },
    'emails.$': {
        type: Object
    },
    'emails.$.address': {
        type: SimpleSchema.RegEx.Email
    },
    'emails.$.verified': {
        type: Boolean
    }
});

// Meteor.users.attachSchema(userSchema);
