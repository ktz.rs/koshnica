import { Mongo } from 'meteor/mongo';
import { FilesCollection } from 'meteor/ostrio:files';

const userDocuments = new FilesCollection({
    collectionName: 'BIOGRAFIJE',
    storagePath: 'data/biographies'
});

const occupations = new Mongo.Collection('occupations');
const schools = new Mongo.Collection('schools');

export { userDocuments, schools, occupations };
